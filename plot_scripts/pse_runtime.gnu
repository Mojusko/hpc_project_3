#!/usr/bin/gnuplot
#
# Creates a version of a plot, which looks nice for inclusion on web pages
#
# AUTHOR: Hagen Wierstorf

reset

#output
set terminal png size 1024,768 enhanced font 'Gill Sans MT,12'
#set terminal svg size 1024,768 fname 'Gill Sans MT' fsize 12 

############### FONTS ############## 
set key font "Gill Sans MT,10"
set xtics font "Gill Sans MT,18" 
set ytics font "Gill Sans MT,18" 
set ylabel font "Gill Sans MT,22" 
set xlabel font "Gill Sans MT,22"
set title font "Gill Sans MT,30"
############ rotate y axis and move########
set ylabel rotate by 0 offset 16,19.5
set xlabel offset -7,0.5
set ytics offset 0,0
#set title offset -60,2
#set format y "10^{%L}"
############## AXES ###################
# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set border 1 back ls 11
set tics nomirror

# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set xtic textcolor '#000000'
set ytic textcolor '#000000'


######################################
######### color definitions###########
######### Tableu 10 ##################
###################################### 
#colors: ['#1F77B4', '#FF7F0E', '#2CA02C', '#D62728', '#9467BD', '#8C564B', '#CFECF9', '#7F7F7F', '#BCBD22', '#17BECF'],
#### pt <-- type of points
#### ps <-- size of points
#### lt <-- line type
#### lw <-- line width 
set style line 1  lc rgb '#1F77B4' pt 1 ps 2 lt 1 lw 3 # --- red
set style line 2  lc rgb '#FF7F0E' pt 2 ps 2 lt 1 lw 2 # --- green
set style line 3  lc rgb '#2CA02C' pt 3 ps 2 lt 1 lw 2 # --- red
set style line 4  lc rgb '#D62728' pt 4 ps 2 lt 1 lw 2 # --- red
set style line 5  lc rgb '#9467BD' pt 5 ps 2 lt 1 lw 2 # --- red
set style line 6  lc rgb '#8C564B' pt 6 ps 2 lt 1 lw 2 # --- red
set style line 7  lc rgb '#CFECF9' pt 7 ps 2 lt 1 lw 2 # --- red
set style line 8  lc rgb '#7F7F7F' pt 8 ps 2 lt 1 lw 2 # --- red
set style line 9  lc rgb '#BCBD22' pt 9 ps 2 lt 1 lw 2 # --- red
set style line 10 lc rgb '#17BECF' pt 10 ps 2 lt 1 lw 2 # --- red

###########################
##### Turn of legend ######
###########################
set key on
set key left
###########################
######### Labels ##########
###########################
set ylabel 'Runtime speedup'
set xlabel 'Grid Size'

###########################
######### Ranges ##########
###########################
#set xrange [100:200]
#set yrange [100:200]

####################################
######### Logarithmic scale ########
####################################
set logscale x
#set logscale y

####################################
######## Set title #################
####################################
set title " "

####################################
####### NAMES OVER DATA ############
####################################

####################################
######### FILE OUTPUT ##############
####################################
set output 'plots/pse_run.png'

####################################
######## Comand to plot data #######
####################################
### One can even enumerate the name with variables from data
## set label 2 '\ft $5$\,meV'         at 1.38,4e9     rotate by  78.5 center tc ls 1
## set label 3 '\ft $10$\,meV'        at 1.24,2e10    rotate by  71.8 center tc ls 2
## set label 4 '\ft $20$\,meV'        at 1.01,9e11    rotate by  58.0 center tc ls
##set label font "Gill Sans MT,15"

#set label 1 'label'  at 1050,8e9     rotate by  0 center tc ls 1
#set label 2 'label'  at 400,1.4e10    rotate by  0 center tc ls 2
###############################################################
##################### MAIN ####################################
################################################################

## use plot "name_of_file" using 1:2 with lines,points,linespoints
## use title "name_of_file" when using legend
## linestile is specified by ls 
#####
set boxwidth 0.95 relative
set style fill transparent solid 0.5 noborder

plot '<(grep Grid: results/pse_profile_run.txt)' u 3:($6/$5) every 3::2 w  boxes ls 3 title "Precomp (Different Flops)",'<(grep Grid: results/pse_profile_run.txt)' u 3:($6/$5) every 3::1 w boxes ls 2 title "ISPC",'<(grep Grid: results/pse_profile_run.txt)' u 3:($6/$5) every 3::0 w boxes ls 1 title "Scalar
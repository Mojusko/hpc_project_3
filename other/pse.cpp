#include <cstdio>
#include <cstdlib>
#include "utilities.hpp"
#include "settings.hpp"
#include <cmath>
#include <cstdlib>
#include <cstring>

inline real_t min(const real_t x, const real_t y) {
	return x < y ? x : y;
}

inline real_t max(const real_t x, const real_t y) {
	return x > y ? x : y;
}

void pse(real_t * const data, const int nX, const int nT, const real_t t, const real_t D, const char fname[], profile_timer_t * const timer, const int nthreads) {
	const real_t dt = t / (nT - 1);
	const real_t h = real_t(1) / (nX - 1);

	const real_t CFL = D * dt / h / h / real_t(2);
	if(CFL > 0.5) {
		printf("CFL condition not satisfied (D * dt / h ** 2 = %4.2f\n", CFL);
		exit(-1);
	}

	const real_t dxdy = h*h;
	const real_t eps = 2*h;
	const real_t threshold = 25*eps*eps;

	real_t * placeholder;
	posix_memalign((void **) &placeholder,  8*sizeof(real_t), 2*nX*nX*sizeof(real_t));		

	real_t (* r)[nX][nX] = (real_t (*)[nX][nX]) placeholder;

	memcpy(&r[0][0][0], data, nX*nX*sizeof(real_t));

	FILE *f = write_header_and_initial_continuous(&r[0][0][0], t, nT, nX, fname);

	timer_tic(timer);
	for(int k = 0; k < nT - 1; ++k) {
		for(int i = 1; i < nX - 1; ++i) {
			for(int j = 1; j < nX - 1; ++j)
			{
				const real_t rho_r = r[k%2][i][j];
				const real_t x_r = i * h;
				const real_t y_r = j * h;

				real_t result(0);
				for (int l = i-10; l < i + 11; ++l) {
					for (int m = j-10; m < j + 11; ++m) {

							int ll, mm;

							// Ghost particles, mirror indices 

							if(l < 0)
								ll = -l;
							else if(l >= nX)
								ll = 2*(nX-1) - l;
							else
								ll = l;

							if(m < 0)
								mm = -m;
							else if(m >= nX)
								mm = 2*(nX-1) - m;
							else
								mm = m;




							real_t rho_p = r[k%2][ll][mm];

							if (l < 0 or l >= nX or m < 0 or m >= nX)
								rho_p *= -1;


							// Distances are non-mirrored, thus l, and m indices
							const real_t x_p = l * h;
							const real_t y_p = m * h;

							const real_t dist_sq = (x_r - x_p)*(x_r - x_p) + (y_r - y_p)*(y_r - y_p);
			
/*							if(dist_sq > threshold)
								continue;*/

							const real_t dist_sq_eps = dist_sq / eps / eps;
							const real_t dist_4th = dist_sq_eps * dist_sq_eps;

							result += (rho_p - rho_r) / (dist_4th*dist_4th + 1);							
					}
				}

				result *= 16 * D * dt * dxdy / eps / eps / eps / eps / M_PI / M_PI;
				r[(k+1)%2][i][j] = rho_r + result;

			}	
		}

		write_data_continuous(&r[(k+1)%2][0][0], nT, nX, f);

	}
	timer_toc(timer);

	memcpy(const_cast<real_t *>(data), &r[(nT-1)%2][0][0], sizeof(real_t)*nX*nX);
	free(placeholder);		

	if(f != NULL)
		fclose(f);	


}

import numpy as np
import sys
import os

def unroll_scalar(UNROLL_SCALAR):

	print("double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*"+str(UNROLL_SCALAR)+");")
	print("double *s_x = (double *) calloc(sizeof(double), (Mx - 2)*"+str(UNROLL_SCALAR)+");")

	print("for (int j = 1; j < My - 1 - "+str(UNROLL_SCALAR)+"; j = j+" + str(UNROLL_SCALAR)+ ") {")
	print("\tfor (int i = 1; i < Mx - 1; i++) {")
	for uu in range(UNROLL_SCALAR):
		print("\tr_x[(Mx-2)*"+str(uu)+"+i - 1] = -rho[i][j + " +str(uu)+ "] - constant * (rho[i][j - 1 + "+str(uu)+"] - 2 * rho[i][j + "+str(uu)+"] + rho[i][j + 1 + "+str(uu)+"]);")
	print("\t}")
	for uu in range(UNROLL_SCALAR):
		print("\tspec_thomas_mult_denom( s_x + (Mx-2)*"+str(uu)+", r_x + (Mx-2)*"+str(uu)+", c_prime_x, c_denom_x, a, b, Mx-2);")
			
	print("for (int i = 1; i < Mx - 1; i++) {")
	for uu in range(UNROLL_SCALAR):
		print("\trho_new[i][j + "+str(uu)+"] = s_x[(Mx-2)*"+str(uu)+"+i - 1];")
	print("\t}")
	print("}")

	##### REST NOT IN UNROLLING

	print("for(int  j = My - 2 - "+str(UNROLL_SCALAR)+"; j < My -1; j++){")
	print("""\tfor (int i = 1; i < Mx - 1; i++) {
	        		r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
	        	}
	        	spec_thomas_mult_denom( s_x, r_x, c_prime_x, c_denom_x, a, b, Mx-2);
	        	for(int i = 1; i < Mx - 1; i++) {
	        	    rho_new[i][j] = s_x[i - 1];    
	        	} 
	        }""")

	################################

	print("double *r_y = (double *) calloc(sizeof(double), (My - 2)*"+str(UNROLL_SCALAR)+");")
	print("for (int i = 1; i < Mx - 1 - "+str(UNROLL_SCALAR)+"; i = i+" + str(UNROLL_SCALAR)+ ") {")
	print("\tfor (int j = 1; j < My - 1; j++) {")
	for uu in range(UNROLL_SCALAR):
		print("\t\tr_y[(My-2)*"+str(uu)+"+j - 1] = -rho_new[i + " +str(uu)+ "][j] - constant * (rho_new[i - 1 + " +str(uu)+ "][j] - 2 * rho_new[i + "+str(uu)+"][j] + rho_new[i + "+str(uu)+" + 1][j]);")
	print("\t}")
	for uu in range(UNROLL_SCALAR):
		print("\tspec_thomas_mult_denom( grid + (i + "+str(uu)+")*Mx + 1 , r_y + (My-2)*"+str(uu)+" , c_prime_y, c_denom_y, a, b, My-2);")
	print("}")

	#### REST NOT IN UNROLLING

	print("for (int  i = Mx - 2 - "+str(UNROLL_SCALAR)+"; i < Mx -1; i++){")
	print("""	for (int j = 1; j < My - 1; j++) {
                	r_y[j - 1] = -rho_new[i][j] - constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);          
            	}
            	spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);  
           	}""")


	return 0

arguments = []
for arg in sys.argv:
	arguments.append(arg)

file_name = arguments[1]
factor = int(arguments[2])
inp = open(file_name, "r")
new_file_name = file_name.split(".cpp")[0] + "_unrolled.cpp"
sys.stdout  = open(new_file_name,"w")
ignore = 0
for line in inp:
	if "INSERTHERE1" in line.split(" "):
		print line,
		unroll_scalar(factor)
		ignore = 1
	elif "STOPHERE1" in line.split(" "):
		ignore = 0
		print line,
	elif ignore == 0:	
		print line,
	else:
		"nothing"

inp.close()
sys.stdout.close()
os.system("mv " + new_file_name + " " + file_name)




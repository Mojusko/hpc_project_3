import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
import sys
import pandas as pd

### agregatte arguements
arguments = []
for arg in sys.argv:
	arguments.append(arg)

file_name = arguments[1]
inp = open(file_name, "r")

#image
data = []
frames = 0
#readline.
for j in inp:
	#print (j)
	line = j
	#line = inp.readline() 
	line.strip()
	frames += 1
	[Mx,My,t] = map(float,line.split(" "))
	i = 0
	#print ("Reading time snapshot", t)
	r = np.empty([Mx,My],dtype = float)
	for i in np.arange(0,My,1):
		line = inp.readline()
		line = line.strip()
		r[:,i] = [float(j) for j in line.split(" ")]
	data.append(r)



fig = plt.figure()
im = plt.imshow(data[0],cmap='hsv', interpolation='nearest')
plt.colorbar()
def animate(i):
	im.set_array(data[i])
	return [im]


anim = animation.FuncAnimation(fig, animate, frames=frames, interval=100, blit=True )

plt.show()



# plt.imshow(r,cmap='hot', interpolation='nearest')
# plt.show()
# plt.close()
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
import sys
import pandas as pd

arguments = []
for arg in sys.argv:
	arguments.append(arg)

file_name = arguments[1]
file_name2 = arguments[2]

inp1 = open(file_name, "r")
inp2 = open(file_name2,"r")

data = []
frames = 0
#readline.
for j in inp1:
	#print (j)
	line = j
	#line = inp.readline() 
	line.strip()
	frames += 1
	[Mx,My,t] = map(float,line.split(" "))
	i = 0
	#print ("Reading time snapshot", t)
	r = np.empty([Mx,My],dtype = float)
	for i in np.arange(0,My,1):
		line = inp1.readline()
		line = line.strip()
		r[:,i] = [float(j) for j in line.split(" ")]
	data.append(r)


data2 = []
frames2 = 0
#readline.
for j in inp2:
	#print (j)
	line = j
	#line = inp.readline() 
	line.strip()
	frames2 += 1
	[Mx,My,t] = map(float,line.split(" "))
	i = 0
	#print ("Reading time snapshot", t)
	r = np.empty([Mx,My],dtype = float)
	for i in np.arange(0,My,1):
		line = inp2.readline()
		line = line.strip()
		r[:,i] = [float(j) for j in line.split(" ")]
	data2.append(r)



## Comaparison
fig, (ax1, ax2) = plt.subplots(1,2)
im1 = ax1.imshow(data[0],cmap='hsv', interpolation='nearest')
im2 = ax2.imshow(data2[0],cmap='hsv', interpolation='nearest')
ax1.set_title(file_name)
ax2.set_title(file_name2)
# ax1.colorbar()
# ax2.colorbar()
def animate(i):
	im1.set_array(data[i])
	im2.set_array(data2[i])
	return [im1,im2]

## Difference Plot 
fig2 = plt.figure()
im = plt.imshow(np.abs(data[0]-data2[0]),cmap='hsv', interpolation='nearest')
plt.colorbar()
def animate2(i):
	im.set_array((np.abs(data[i]-data2[i]))/np.abs(data[i]))
	return [im]

anim1 = animation.FuncAnimation(fig2, animate2, frames=frames, interval=100, blit=True )
anim2 = animation.FuncAnimation(fig, animate, frames=frames, interval=100, blit=True )

## Saving 
if "-save" in arguments:
	index=arguments.index("-save")
	name_of_output=arguments[index+1]

	Writer = animation.writers['ffmpeg']
	writer = Writer(fps=35, bitrate=None, codec="libx264", extra_args=['-pix_fmt', 'yuv420p'])
	anim1.save(name_of_output, writer=writer, dpi=100)
	anim2.save("diff" + name_of_output, writer=writer, dpi=100)

plt.show()


import numpy as np
import sys
import os

def unroll_ispc_1(UNROLL_ISPC):
	print("""for (uniform int k = 0; k < UNROLL; k = k + """ + str(UNROLL_ISPC) + """){\n\t foreach (i = 1 ... Mx-1){	""")
	for uu in range(UNROLL_ISPC):
		print("\t\tconst double up" +  str(uu)+" = rho_cen[i*My+k-1 + " + str(uu) + "];")
		print("\t\tconst double mid" +  str(uu)+" = rho_cen[i*My+k + " + str(uu) + "];")
		print("\t\tconst double down" +  str(uu)+" = rho_cen[i*My+k+1 + " + str(uu) + "];")
		print("\t\tconst double whole" + str(uu)+" =  ( up" + str(uu)+" - 2* mid" + str(uu)+" + down" + str(uu)+" )  * -1.* constant ;")
		print("\t\tconst double res" + str(uu)+" = whole" + str(uu)+" - mid" + str(uu)+";")
		print("\t\trx[ (Mx-2)*(k + " + str(uu)+" ) +(i-1)]  	  = res" + str(uu)+" ;")
	print("\t}")
	print("}")
	return 0

def unroll_ispc_2(UNROLL_ISPC):
	print("""for (uniform int k = 0; k < UNROLL; k = k + """ + str(UNROLL_ISPC) + """){\n\t foreach (i = 1 ... My-1){	""")
	for uu in range(UNROLL_ISPC):
		print("\t\tconst double up" +  str(uu)+" = rho_cen[(Mx)*(k-1 +" + str(uu) + ") + i ];")
		print("\t\tconst double mid" +  str(uu)+" = rho_cen[(Mx)*(k +" + str(uu) + ") + i ];")
		print("\t\tconst double down" +  str(uu)+" = rho_cen[(Mx)*(k+1+" + str(uu) + ") + i ];")
		print("\t\tconst double whole" + str(uu)+" =  ( up" + str(uu)+" - 2* mid" + str(uu)+" + down" + str(uu)+" )  * -1.* constant ;")
		print("\t\tconst double res" + str(uu)+" = whole" + str(uu)+" - mid" + str(uu)+";")
		print("\t\try[ (My-2)*(k + " + str(uu)+" ) +(i-1)]  	  = res" + str(uu)+" ;")
	print("\t}")
	print("}")
	return 0

arguments = []
for arg in sys.argv:
	arguments.append(arg)

file_name = arguments[1]
factor = int(arguments[2])
inp = open(file_name, "r")
new_file_name = file_name.split(".ispc")[0] + "_unrolled.ispc"
sys.stdout  = open(new_file_name,"w")
ignore = 0
for line in inp:
	if "INSERTHERE1" in line.split(" "):
		print line,
		unroll_ispc_1(factor)
		ignore = 1
	elif "STOPHERE1" in line.split(" "):
		ignore = 0
		print line,
	elif ignore == 0:	
		print line,
	else:
		"nothing"

inp.close()
sys.stdout.close()
os.system("mv " + new_file_name + " " + file_name)


inp = open(file_name, "r")
new_file_name = file_name.split(".ispc")[0] + "_unrolled.ispc"
sys.stdout  = open(new_file_name,"w")
ignore = 0
for line in inp:
	if "INSERTHERE2" in line.split(" "):
		print line,
		unroll_ispc_2(factor)
		ignore = 1
	elif "STOPHERE2" in line.split(" "):
		ignore = 0
		print line,
	elif ignore == 0:	
		print line,
	else:
		"nothing"

inp.close()
sys.stdout.close()
os.system("mv " + new_file_name + " " + file_name)




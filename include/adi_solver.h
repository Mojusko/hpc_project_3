//
// Created by mojko on 7/21/16.
//

/*
 *  Solve_dif(mesh, std vector params (includes D, M, space_step , time_step, print and save options), data_pointer_for_storage)
 *
 */


#ifndef PROJECT_3_ADI_SOLVER_H
#define PROJECT_3_ADI_SOLVER_H
 
#include "param.h"
#include "helper.h"
#include "linear_algebra.h"
#include <functional>
#include "adi_stencil.h"
#include "meta_params.h"
#include "adi_full.h"
#include <omp.h>
#include <immintrin.h>
#include <stdlib.h>
class adi_solver {

public:
    adi_solver();
    ~adi_solver();

    void solve_vector_int(double *, param &);
    void solve_vector_int_tlp(double *, param&);

    void solve_vector_ispc(double * ,  param&);
    void solve_vector_ispc_tlp(double * ,  param&);

    void solve_vector_ispc2(double * ,  param&);
    void solve_vector_ispc2_tlp(double *,param &);

    void solve_scalar(double * grid, param & param_vec );
    void solve_scalar_tlp(double * grid, param & param_vec);

    void solve_scalar_unroll(double * grid, param & param_vec );
    void solve_scalar_unroll_tlp(double * grid, param & param_vec );

};


#endif //PROJECT_3_ADI_SOLVER_H

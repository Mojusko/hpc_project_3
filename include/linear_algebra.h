//
// Created by mojko on 7/26/16.
//

#ifndef PROJECT_3_LINEAR_ALGEBRA_H
#define PROJECT_3_LINEAR_ALGEBRA_H
#include <iostream>
//#include "parameters.h"

void spec_thomas_invert(double a, double b, double c, double *c_prime, int n);
void spec_thomas_denom(double a, double b, double *c_prime, double *denom, int n);

inline void spec_thomas_mult(double *x,double *d, double *c_prime, double a, double b, int n){
	d[0] = d[0]/b;

    for (int i = 1; i<n; i++){
        d[i] =  (d[i] - a*d[i-1])/(b- a*c_prime[i-1]);
    }

    x[n-1] = d[n-1];
    for (int i = n-2; i>=0 ; i--){
        x[i] = d[i]- c_prime[i]*x[i+1];
    }
    
}

inline void spec_thomas_mult_denom(double *x,double *d, double *c_prime, double* denom, double a, double b, int n){
	d[0] = d[0]/b;

    for (int i = 1; i<n; i++){
        d[i] =  (d[i] - a*d[i-1])*(denom[i-1]);
    }

    x[n-1] = d[n-1];
    for (int i = n-2; i>=0 ; i--){
        x[i] = d[i]- c_prime[i]*x[i+1];
    }
    
}


//void thomas_solve(Eigen::MatrixXd & A, Eigen::VectorXd & r, Eigen::VectorXd & x);

#endif //PROJECT_3_LINEAR_ALGEBRA_H

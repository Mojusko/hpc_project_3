//
// Created by mojko on 7/22/16.
//

#ifndef PROJECT_3_PARAM_H
#define PROJECT_3_PARAM_H

#include <iostream>
#include "timing.h"

class param {

public:
    param();
    ~param();
    void set_params(double, double, double, int, int, double, double, double, int, double *, int, std::string, int);
    void set_timer(profile_timer_t &);
    
    void dealloc();
    double D = 1.;
    profile_timer_t timer; 
    bool timer_on = false; 
    int Mx = 100;
    int My = 100;
    double x_start = 0;
    double y_start = 0;
    int mode = 1; 
    double total_time = 1;
    double space_step = 0.1;
    double time_step = 0.1;
    int print_opt = 0;
    double *trajectory;
    int save_opt = 0;
    std::string save_file;

};


#endif //PROJECT_3_PARAM_H

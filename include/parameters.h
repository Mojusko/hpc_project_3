//
// Created by mojko on 7/21/16.
//

#ifndef PROJECT_3_PARAMETERS_H_H
#define PROJECT_3_PARAMETERS_H_H

typedef double real_t;
// PARAMETERS

const int Mx = (int) 100;
const int My = (int) 100;
const double delta_t  = 0.05;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;

const double delta_x = 1./(Mx - 1);
const double delta_y = 1./(My - 1);

const double total_t = 1.;
const double Dconst = 0.1;




#endif //PROJECT_3_PARAMETERS_H_H

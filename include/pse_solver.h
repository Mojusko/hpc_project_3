//
// Created by mojko on 7/26/16.
//

#ifndef PROJECT_3_PSE_SOLVER_H
#define PROJECT_3_PSE_SOLVER_H
#include "param.h"
#include "helper.h"
#include <functional>
#include "pse_stencil.h"
#include "meta_params.h"
#include <stdlib.h>

class pse_solver {
public:

    pse_solver();
    ~pse_solver();

    void solve(double *grid, param & param_vec);

private:
    void step_scalar(double *grid, double *mem,  param & param_vec);
    void step_scalar_imp(double *grid, double *mem,  param  & param_vec);
    void step_vector_ispc(double *grid, double *mem,  param & param_vec);
    void step_vector_ispc2(double *grid, double *mem,  param & param_vec);

    void step_scalar_tlp(double *grid, double *mem,  param & param_vec);
    void step_vector_ispc_tlp(double *grid, double *mem,   param & param_vec);
    void step_vector_ispc2_tlp(double *grid, double *mem,  param & param_vec);

    void step_vector_ispc2_precomp(double *grid, double *mem, param  & param_vec);
};


#endif //PROJECT_3_PSE_SOLVER_H

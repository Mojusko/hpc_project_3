//
// Created by mojko on 7/29/16.
//

#ifndef PROJECT_3_TIMING_H
#define PROJECT_3_TIMING_H
#include "meta_params.h"
#include <chrono>
#include <iostream>

typedef struct {
    std::chrono::time_point<std::chrono::steady_clock> start, end;
    double diff[2];
    int count; 
    int runs = -1;
} profile_timer_t;

void timer_tic(profile_timer_t &  timer);

void timer_toc(profile_timer_t &  timer);

void timer_reset(profile_timer_t &  timer);

double timer_clock_diff(profile_timer_t & timer);

double timer_clock_diff_2(profile_timer_t & timer);




#endif //PROJECT_3_TIMING_H

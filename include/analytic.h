//
// Created by mojko on 7/24/16.
//

#ifndef PROJECT_3_ANALYTIC_H
#define PROJECT_3_ANALYTIC_H

#include "helper.h"
#include <cmath>
#include "param.h"
#include <iostream>
#include "omp.h"

class analytic {
public:
    void evaluate(double t, double * grid, double, int, int, double, double,double );
    void evaluate_trajectory(double * grid, param param_vec);

};


#endif //PROJECT_3_ANALYTIC_H

//
// Created by mojko on 7/24/16.
//

#ifndef PROJECT_3_HELPER_H
#define PROJECT_3_HELPER_H
#include <iostream>
#include <cmath>
#include "meta_params.h"

void print_grid_to_file(std::string, double *, int, double);
void copy_2d(double *, double *, int, int);
void compare_2d(double *, double *, int, int , int);
void set_bc(double *,double, double, double, double, int, int);
void set_ic(double *,double, double, double, double, int, int);
void set_ic_box(double *,double, double, double, double, int, int);
void test_max_norm(double * traj1, double * traj2, int Mx, int My, double h, double delta_t, int T);
void precompute_matrix(double * matrix, double h, double eps);

#endif //PROJECT_3_HELPER_H

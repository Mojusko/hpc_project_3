//
// Created by mojko on 7/28/16.
//

#ifndef PROJECT_3_META_PARAMS_H
#define PROJECT_3_META_PARAMS_H

#define ISPC 1

//#define VERBOSE 1

#define OPENMP 1
#define INTRINSICS 1
#define TIME_MEASUREMENT 1

#define VECSIZE 4


// Size of Kernel around, must be at least as big as smallest grid
#define SIZE 16

// Tells the size that goes to ISPC kernel at once for ADI, must be multiple of UNROLL IN ISPC
#define UNROLL 32

// Processor frequency of the current system
#define FREQUENCY 2.1e9
// Parameter needed for some systems in order to get proper results for clock()
#define FREQUENCY_COEFFICIENT 2100


#endif //PROJECT_3_META_PARAMS_H
//
// Created by mojko on 7/24/16.
//
#include "helper.h"


void print_grid_to_file(std::string file_name, double * grid, int size, double t){
    double (* rho)[size] = (double (*)[size]) grid;

    FILE *f;
    f = fopen(file_name.c_str(), "a");
    if (f == NULL)
    {
        printf("### Error opening file!\n");
        exit(1);
    }
    else {
        //printf("### Saving to a file!\n");

        fprintf(f,"%d %d %f\n", size, size, t);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                fprintf(f,"%4.4f ", rho[i][j]);
            }
            fprintf(f,"\n");
        }

    }
    fclose(f);

};


void copy_2d(double * past, double * future, int Mx, int My){
    //double (* past2)[My] = (double (*)[My]) past;

    for (int i = 0; i < Mx; i++){
        for (int j =0; j<My; j++){
            *(future + Mx*i + j) = *(past + Mx*i + j);
        }
    }

}

void compare_2d(double * traj1, double * traj2, int Mx, int My, int T){



    FILE *f;
    f = fopen("log.txt", "a");
    if (f == NULL)
    {
        printf("### Error opening file!\n");
        exit(1);
    }
    else {

            double max_rel = 0;
        for (int r = 0; r < T; r++) {
            double val = 0;
            double rel = 0;
            for (int i = 0; i < Mx; i++) {
                for (int j = 0; j < My; j++) {
                    //printf("%f %f\n",*(traj1 +  r*Mx*My + Mx*i + j ),*(traj2 +  r*Mx*My + Mx*i + j ));
                    double d = std::abs(*(traj1 + r * Mx * My + Mx * i + j) - *(traj2 + r * Mx * My + Mx * i + j));
                    double total_max = std::max(std::abs(*(traj1 + r * Mx * My + Mx * i + j)),
                                              std::abs(*(traj2 + r * Mx * My + Mx * i + j)));
                    rel = std::max(rel,
                                   std::abs(*(traj1 + r * Mx * My + Mx * i + j) - *(traj2 + r * Mx * My + Mx * i + j)) /
                                   total_max);

                    val = std::max(val, d);
                }
            }
            max_rel = std::max(max_rel,rel * 100);
            fprintf(f,"Differece at %d : %5.5f, Relative: %5.5f perc. \n", r, val, rel * 100);
            val = 0;
        }
        printf("### Max Relative Difference: %5.5f \n", max_rel);
    }
}



void test_max_norm(double * traj1, double * traj2, int Mx, int My, double h, double delta_t, int T){
        double val = 0;
        for (int i = 0; i < Mx; i++) {
            for (int j = 0; j < My; j++) {
                double d = std::abs(*(traj1 + + Mx * i + j) - *(traj2 + Mx * i + j));
                val = std::max(val, d);
            }
        }

        printf("%10.10f \t %10.10f \t %10.10f \n",h,delta_t,val);
    
}




void set_bc(double * mem,double xstart, double ystart, double dx, double dy, int Mx, int My){
    double (* rho)[My] = (double (*)[My]) mem;

/*
    auto trans_x = [dx,xstart](int j) {
        return (double)  dx * (double) j + xstart;
    };
    auto trans_y = [dy,ystart] (int j) {
        return (double) dy * (double) j + ystart;
    };
*/


    for (int i = 0; i<Mx; i++) {
        rho[i][0] = 0.;
        rho[i][My-1] = 0.;
    }

    for (int j = 0; j<My; j++){
        rho[0][j] = 0.;
        rho[Mx-1][j] = 0.;
    }

    std::cout << "### BC set." << std::endl;

}

void set_ic(double * mem,double xstart, double ystart, double dx, double dy, int Mx, int My){
    double (* rho)[My] = (double (*)[My]) mem;

    auto trans_x = [dx,xstart](int j) {
        return (double)  dx * (double) j + xstart;
    };

    auto trans_y = [dy,ystart] (int j) {
        return (double) dy * (double) j + ystart;
    };

    for (int i = 1; i<(Mx-1); i++){
        for (int j = 1; j<(My-1); j++){
            rho[i][j] = std::sin(M_PI*trans_x(i))*std::sin(M_PI*trans_y(j));
        }
    }

    std::cout << "### IC set." << std::endl;

}



void set_ic_box(double * mem,double xstart, double ystart, double dx, double dy, int Mx, int My){
    double (* rho)[My] = (double (*)[My]) mem;

    auto trans_x = [dx,xstart](int j) {
        return (double)  dx * (double) j + xstart;
    };

    auto trans_y = [dy,ystart] (int j) {
        return (double) dy * (double) j + ystart;
    };

    for (int i = 1; i<(Mx-1); i++){
        for (int j = 1; j<(My-1); j++){

            if (trans_x(i)>0.05 and trans_x(i)<0.75 and trans_y(j)>0.25 and trans_y(j)<0.75)
            {
                rho[i][j] = 1;
            }
        }
    }

    std::cout << "### IC set." << std::endl;

}


void precompute_matrix(double * matrix, double h, double eps){
    int size = SIZE; 
    for (int i = 0; i < size*2 + 1;i++){
        for(int j = 0; j< size*2 +1; j++){
            double val = h*h*((i-size)*(i-size) + (j-size)*(j-size));
            val = val/(eps*eps);
            val = val*val*val*val;
            *(matrix + (2*size+1)*i + j ) = 1./(1. + val);
        }
    }
}
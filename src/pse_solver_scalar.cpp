#include "pse_solver.h"

__attribute__((optimize("no-tree-vectorize")))
void pse_solver::step_scalar(double *grid, double *mem, param & param_vec){

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;

/*    const double xstart = param_vec.x_start;
    const double ystart = param_vec.y_start;
*/
    const double D = param_vec.D;
    const double h = param_vec.space_step;
    const double dt = param_vec.time_step;

    //double *init = mem;
    double (*rho_new)[My] = (double (*)[My]) mem;
    double (*rho)[My] = (double (*)[My]) grid;

    const double eps = 2 * h;
    const double sigma = (dt * D / (eps * eps)) * h * h;
    const double C = (16./(eps*eps*M_PI*M_PI));
    const double constant = sigma*C;
    //const int SIZE = 5 * (int) (eps/h);
    const double inv_eps = 1./(eps*eps);

    for (int i = 1; i < Mx - 1; i++) {
        const double x_ij = i*h;// + xstart;
        for (int j = 1; j < My - 1; j++) {

            const double val = rho[i][j];
            double update = 0.0;
            const double y_ij = j*h;// + ystart;

            for (int k = i - SIZE; k <= i + SIZE; k++) {
                const double x_p = k*h;// + xstart;

                for (int r = j - SIZE; r <= j + SIZE; r++) {

                    // Ghost particles
                    double pref = 1.0;
                    int kn = k;
                    int rn = r;
                    
                    if ((k < 0 || k > Mx - 1) || (r < 0 || r > My - 1)){
                        pref = -1.0;

                        if (k < 0) {
                            kn = -k;
                        }
                        else {
                            if (k >= Mx) {
                                kn = 2 * (Mx - 1) - k;
                            }
                        }

                        if (r < 0) {
                            rn = -r;
                        }
                        else {
                            if (r >= My) {
                                rn = 2 * (My - 1) - r;
                            }
                        }
                    }
                    // -------


                    const double y_p = r*h;// + ystart;
                    const double rz = (x_ij - x_p) * (x_ij - x_p) + (y_ij - y_p) * (y_ij - y_p);
                    const double rad = rz * inv_eps;
                    const double r_to_4 = rad * rad * rad * rad;
                    const double z =  ( 1. / (1. + r_to_4));

                    update += ((pref * rho[kn][rn]) - val) * z;
                }

                rho_new[i][j] = val + constant*update;

            }
        }

    }
}




__attribute__((optimize("no-tree-vectorize")))
void pse_solver::step_scalar_imp(double *grid, double *mem, param & param_vec){

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;

    const double xstart = param_vec.x_start;
    const double ystart = param_vec.y_start;

    const double D = param_vec.D;
    const double h = param_vec.space_step;
    const double dt = param_vec.time_step;

    //double *init = mem;
    double (*rho_new)[My] = (double (*)[My]) mem;
    double (*rho)[My] = (double (*)[My]) grid;

    const double eps = 2 * h;
    const double sigma = (dt * D / (eps * eps)) * h * h;
    const double C = (16./(eps*eps*M_PI*M_PI));
    const double constant = sigma*C;
    const double inv_eps = 1./(eps*eps);
    

    const int block_size = 8; 
    const int blocks = (Mx - 2)/ block_size; 

    for (int ib = 0; ib < blocks; ib++){
        for( int jb = 0; jb < blocks; jb++){          

            for (int i = block_size*ib +1; i < block_size*(ib+1) +1 ; i++) {
                const double x_ij = i*h + xstart;
                for (int j = block_size*jb +1 ;j < block_size*(jb+1) +1; j++) {

                    const double val = rho[i][j];
                    double update = 0.0;
                    const double y_ij = j*h + ystart;

                    for (int k = i - SIZE; k <= i + SIZE; k++) {
                        const double x_p = k*h + xstart;

                        for (int r = j - SIZE; r <= j + SIZE; r++) {

                            // Ghost particles
                            int kn = k;
                            int rn = r;
                            double pref = 1.0;
                            if ((k < 0 || k > Mx - 1) || (r < 0 || r > My - 1)){
                                pref = -1.0;


                                if (k < 0) {
                                    kn = -k;
                                }
                                else {
                                    if (k >= Mx) {
                                        kn = 2 * (Mx - 1) - k;
                                    }
                                }

                                if (r < 0) {
                                    rn = -r;
                                }
                                else {
                                    if (r >= My) {
                                        rn = 2 * (My - 1) - r;
                                    }
                                }
                            }
                            // -------


                            const double y_p = r*h + ystart;

                            double rad = (x_ij - x_p)*(x_ij-x_p) + (y_ij - y_p)*(y_ij - y_p);
                            rad = rad*inv_eps;
                            const double r_to_4 = rad*rad*rad*rad;
                            const double z =  ( 1. / (1. + r_to_4));

                            update += ((pref * rho[kn][rn]) - val) * z;
                        }

                        rho_new[i][j] = val + constant*update;
                    }
                }
            }





        }


            for (int i = block_size*ib + 1 ; i < std::min(block_size*(ib+1),Mx-1) ; i++) {
                const double x_ij = i*h + xstart;
                for (int j = 1  ;j < My-1 ; j++) {

                    const double val = rho[i][j];
                    double update = 0.0;
                    const double y_ij = j*h + ystart;

                    for (int k = i - SIZE; k <= i + SIZE; k++) {
                        const double x_p = k*h + xstart;

                        for (int r = j - SIZE; r <= j + SIZE; r++) {

                            // Ghost particles
                            int kn = k;
                            int rn = r;
                            double pref = 1.0;
                            if ((k < 0 || k > Mx - 1) || (r < 0 || r > My - 1)){
                                pref = -1.0;


                                if (k < 0) {
                                    kn = -k;
                                }
                                else {
                                    if (k >= Mx) {
                                        kn = 2 * (Mx - 1) - k;
                                    }
                                }

                                if (r < 0) {
                                    rn = -r;
                                }
                                else {
                                    if (r >= My) {
                                        rn = 2 * (My - 1) - r;
                                    }
                                }
                            }
                            // -------


                            const double y_p = r*h + ystart;

                            double rad = (x_ij - x_p)*(x_ij-x_p) + (y_ij - y_p)*(y_ij - y_p);
                            rad = rad*inv_eps;
                            const double r_to_4 = rad*rad*rad*rad;
                            const double z =  ( 1. / (1. + r_to_4));

                            update += ((pref * rho[kn][rn]) - val) * z;
                        }

                        rho_new[i][j] = val + constant*update;
                    }
                }
            }
    }

    for (int i = block_size*(blocks) ; i < Mx -1; i++) {
            const double x_ij = i*h + xstart;
                for (int j = 1  ;j < My-1 ; j++) {

                    const double val = rho[i][j];
                    double update = 0.0;
                    const double y_ij = j*h + ystart;

                    for (int k = i - SIZE; k <= i + SIZE; k++) {
                        const double x_p = k*h + xstart;

                        for (int r = j - SIZE; r <= j + SIZE; r++) {

                            // Ghost particles
                            int kn = k;
                            int rn = r;
                            double pref = 1.0;
                            if ((k < 0 || k > Mx - 1) || (r < 0 || r > My - 1)){
                                pref = -1.0;


                                if (k < 0) {
                                    kn = -k;
                                }
                                else {
                                    if (k >= Mx) {
                                        kn = 2 * (Mx - 1) - k;
                                    }
                                }

                                if (r < 0) {
                                    rn = -r;
                                }
                                else {
                                    if (r >= My) {
                                        rn = 2 * (My - 1) - r;
                                    }
                                }
                            }
                            // -------


                            const double y_p = r*h + ystart;

                            double rad = (x_ij - x_p)*(x_ij-x_p) + (y_ij - y_p)*(y_ij - y_p);
                            rad = rad*inv_eps;
                            const double r_to_4 = rad*rad*rad*rad;
                            const double z =  ( 1. / (1. + r_to_4));

                            update += ((pref * rho[kn][rn]) - val) * z;
                        }

                        rho_new[i][j] = val + constant*update;
                    }
                }
            }




}















void pse_solver::step_scalar_tlp(double *grid, double *mem, param & param_vec){

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;

    const double xstart = param_vec.x_start;
    const double ystart = param_vec.y_start;

    const double D = param_vec.D;
    const double h = param_vec.space_step;
    const double dt = param_vec.time_step;

    //double *init = mem;
    double (*rho_new)[My] = (double (*)[My]) mem;
    double (*rho)[My] = (double (*)[My]) grid;

    const double eps = 2 * h;
    const double sigma = (dt * D / (eps * eps)) * h * h;
    const double C = (16./(eps*eps*M_PI*M_PI));
    const double constant = sigma*C;
    //const int SIZE = 5 * (int) (eps/h);
    double inv_eps = 1./(eps*eps);
    
    #pragma omp for
    for (int i = 1; i < Mx - 1; i++) {
        const double x_ij = i*h + xstart;
        for (int j = 1; j < My - 1; j++) {

            const double val = rho[i][j];
            double update = 0.0;
            const double y_ij = j*h + ystart;
            
            for (int k = i - SIZE; k <= i + SIZE; k++) {
                const double x_p = k*h + xstart;
                for (int r = j - SIZE; r <= j + SIZE; r++) {

                    // Ghost particles
                    double pref = 1.0;   
                    int kn = k;
                    int rn = r;
                    if ((k < 0 || k > Mx - 1) || (r < 0 || r > My - 1)){
                        pref = -1.0;

                        if (k < 0) {
                            kn = -k;
                        }
                        else {
                            if (k >= Mx) {
                                kn = 2 * (Mx - 1) - k;
                            }
                        }

                        if (r < 0) {
                            rn = -r;
                        }
                        else {
                            if (r >= My) {
                                rn = 2 * (My - 1) - r;
                            }
                        }
                     }
                    // -------


                    const double y_p = r*h + ystart;

                    double rad = (x_ij - x_p)*(x_ij-x_p) + (y_ij - y_p)*(y_ij - y_p);
                    rad = rad * inv_eps;
                    const double r_to_4 = rad*rad*rad*rad;
                    const double z =  ( 1. / (1. + r_to_4));

                    update += ((pref * rho[kn][rn]) - val) * z;
                }

                rho_new[i][j] = val + constant*update;

            }
        }

    }
}
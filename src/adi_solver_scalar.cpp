#include "adi_solver.h"


__attribute__((optimize("no-tree-vectorize")))
void adi_solver::solve_scalar(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);

    double (*rho_new)[My] = (double (*)[My]) mem;


    for (int i = 0; i < Mx; i++) {
        for (int j = 0; j < My; j++) {
            rho_new[i][j] = rho[i][j];
        }
    }


    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;


    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);
    const double inv_b = 1./b; 
    double *s_x = (double *) calloc(sizeof(double), Mx - 2);
    double *r_x = (double *) calloc(sizeof(double), Mx - 2);
    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);

    double *r_y = (double *) calloc(sizeof(double), My - 2);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);


    while (t <= finish) {

        if (param_vec.save_opt == 1) {
            copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
            counter += 1;
            print_grid_to_file(param_vec.save_file, grid, Mx, t);
        }
        else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
        }
        #ifdef VERBOSE
                printf("### Time: %f\n", t);
        #endif

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_tic(param_vec.timer);
            }
        #endif

        // ------------- Implicit x ----------------------

        for (int j = 1; j < My - 1; j++) {
            for (int i = 1; i < Mx - 1; i++) {
                r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
            }

            r_x[0] = r_x[0]*inv_b;
            for (int i = 1; i<Mx-2; i++){
                r_x[i] =  (r_x[i] - a*r_x[i-1])*(c_denom_x[i-1]);
            }

            rho_new[Mx-2][j] = r_x[Mx-2-1];
            for (int i = Mx-2-2; i>=0 ; i--){
                rho_new[i+1][j] = r_x[i]- c_prime_x[i]*rho_new[i+2][j];
            }

        }


        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

        // ------------- Implicit y --------------------------

        for (int i = 1; i < Mx - 1; i++) {
            for (int j = 1; j < My - 1; j++) {

                r_y[j - 1] = -rho_new[i][j] -
                             constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);
            
            }
            spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);
            // no copy needed as no strided access
        }


     /*   for (int j = 1; j < My - 1; j++) {
            for (int i = 1; i < Mx - 1; i++) {
                rho_new[i][j] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
            }

            rho_new[1][j] = rho_new[1][j]*inv_b;
            for (int i = 1; i<Mx-2; i++){
                rho_new[i+1][j] =  (rho_new[i+1][j] - a*rho_new[i][j])*(c_denom_x[i-1]);
            }

            //rho_new[Mx-2][j] = r_x[Mx-2-1];
            for (int i = Mx-2-2; i>=0 ; i--){
                rho_new[i+1][j] = rho_new[i+1][j]- c_prime_x[i]*rho_new[i+2][j];
            }

        }


        // ------------- Implicit y --------------------------

        for (int i = 1; i < Mx - 1; i++) {
            for (int j = 1; j < My - 1; j++) {

                rho[i][j] = -rho_new[i][j] -
                             constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);
            
            }
            spec_thomas_mult_denom( grid + i*Mx + 1 , grid + i*Mx + 1, c_prime_y, c_denom_y, a, b, My-2);
            // no copy needed as no strided access
        }*/

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

        t = t + dt;


    }
    printf("### Simulation Stopped with ADI\n");

    free(r_x);
    free(r_y);
    free(s_x);
    free(c_prime_x);
    free(c_prime_y);

    free(mem);
}






















__attribute__((optimize("no-tree-vectorize")))
void adi_solver::solve_scalar_unroll(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);

    double (*rho_new)[My] = (double (*)[My]) mem;

    for (int i = 0; i < Mx; i++) {
        for (int j = 0; j < My; j++) {
            rho_new[i][j] = rho[i][j];
        }
    }

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;


    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);


double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*8);
double *s_x = (double *) calloc(sizeof(double), (Mx - 2)*8);
double *r_y = (double *) calloc(sizeof(double), (My - 2)*8);

    while (t <= finish) {

        if (param_vec.save_opt == 1) {
            copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
            counter += 1;
            print_grid_to_file(param_vec.save_file, grid, Mx, t);
        }
        else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
        }
        #ifdef VERBOSE
                printf("### Time: %f\n", t);
        #endif

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_tic(param_vec.timer);
            }
        #endif
            
        // ----------------------- INSERTHERE1 -------------------------------


int resx = (My - 2)/8;
int resy = (Mx - 2)/8;

for (int j = 1; j < resx*8 + 1; j = j+8) {
	for (int i = 1; i < Mx - 1; i++) {
	r_x[(Mx-2)*0+i - 1] = -rho[i][j + 0] - constant * (rho[i][j - 1 + 0] - 2 * rho[i][j + 0] + rho[i][j + 1 + 0]);
	r_x[(Mx-2)*1+i - 1] = -rho[i][j + 1] - constant * (rho[i][j - 1 + 1] - 2 * rho[i][j + 1] + rho[i][j + 1 + 1]);
	r_x[(Mx-2)*2+i - 1] = -rho[i][j + 2] - constant * (rho[i][j - 1 + 2] - 2 * rho[i][j + 2] + rho[i][j + 1 + 2]);
	r_x[(Mx-2)*3+i - 1] = -rho[i][j + 3] - constant * (rho[i][j - 1 + 3] - 2 * rho[i][j + 3] + rho[i][j + 1 + 3]);
	r_x[(Mx-2)*4+i - 1] = -rho[i][j + 4] - constant * (rho[i][j - 1 + 4] - 2 * rho[i][j + 4] + rho[i][j + 1 + 4]);
	r_x[(Mx-2)*5+i - 1] = -rho[i][j + 5] - constant * (rho[i][j - 1 + 5] - 2 * rho[i][j + 5] + rho[i][j + 1 + 5]);
	r_x[(Mx-2)*6+i - 1] = -rho[i][j + 6] - constant * (rho[i][j - 1 + 6] - 2 * rho[i][j + 6] + rho[i][j + 1 + 6]);
	r_x[(Mx-2)*7+i - 1] = -rho[i][j + 7] - constant * (rho[i][j - 1 + 7] - 2 * rho[i][j + 7] + rho[i][j + 1 + 7]);
	}
    
	spec_thomas_mult_denom( s_x + (Mx-2)*0, r_x + (Mx-2)*0, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*1, r_x + (Mx-2)*1, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*2, r_x + (Mx-2)*2, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*3, r_x + (Mx-2)*3, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*4, r_x + (Mx-2)*4, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*5, r_x + (Mx-2)*5, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*6, r_x + (Mx-2)*6, c_prime_x, c_denom_x, a, b, Mx-2);
	spec_thomas_mult_denom( s_x + (Mx-2)*7, r_x + (Mx-2)*7, c_prime_x, c_denom_x, a, b, Mx-2);

for (int i = 1; i < Mx - 1; i++) {
	rho_new[i][j + 0] = s_x[(Mx-2)*0+i - 1];
	rho_new[i][j + 1] = s_x[(Mx-2)*1+i - 1];
	rho_new[i][j + 2] = s_x[(Mx-2)*2+i - 1];
	rho_new[i][j + 3] = s_x[(Mx-2)*3+i - 1];
	rho_new[i][j + 4] = s_x[(Mx-2)*4+i - 1];
	rho_new[i][j + 5] = s_x[(Mx-2)*5+i - 1];
	rho_new[i][j + 6] = s_x[(Mx-2)*6+i - 1];
	rho_new[i][j + 7] = s_x[(Mx-2)*7+i - 1];
	}
}

for(int  j = resx*8 ; j < My -1; j++){     for (int i = 1; i < Mx - 1; i++) {
r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j
+ 1]);                 }                 spec_thomas_mult_denom( s_x, r_x,
c_prime_x, c_denom_x, a, b, Mx-2);                 for(int i = 1; i < Mx - 1;
i++) {                     rho_new[i][j] = s_x[i - 1];                     }
}



        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

//double *r_y = (double *) calloc(sizeof(double), (My - 2)*8);
for (int i = 1; i < resy*8 +1; i = i+8) {
	for (int j = 1; j < My - 1; j++) {
		r_y[(My-2)*0+j - 1] = -rho_new[i + 0][j] - constant * (rho_new[i - 1 + 0][j] - 2 * rho_new[i + 0][j] + rho_new[i + 0 + 1][j]);
		r_y[(My-2)*1+j - 1] = -rho_new[i + 1][j] - constant * (rho_new[i - 1 + 1][j] - 2 * rho_new[i + 1][j] + rho_new[i + 1 + 1][j]);
		r_y[(My-2)*2+j - 1] = -rho_new[i + 2][j] - constant * (rho_new[i - 1 + 2][j] - 2 * rho_new[i + 2][j] + rho_new[i + 2 + 1][j]);
		r_y[(My-2)*3+j - 1] = -rho_new[i + 3][j] - constant * (rho_new[i - 1 + 3][j] - 2 * rho_new[i + 3][j] + rho_new[i + 3 + 1][j]);
		r_y[(My-2)*4+j - 1] = -rho_new[i + 4][j] - constant * (rho_new[i - 1 + 4][j] - 2 * rho_new[i + 4][j] + rho_new[i + 4 + 1][j]);
		r_y[(My-2)*5+j - 1] = -rho_new[i + 5][j] - constant * (rho_new[i - 1 + 5][j] - 2 * rho_new[i + 5][j] + rho_new[i + 5 + 1][j]);
		r_y[(My-2)*6+j - 1] = -rho_new[i + 6][j] - constant * (rho_new[i - 1 + 6][j] - 2 * rho_new[i + 6][j] + rho_new[i + 6 + 1][j]);
		r_y[(My-2)*7+j - 1] = -rho_new[i + 7][j] - constant * (rho_new[i - 1 + 7][j] - 2 * rho_new[i + 7][j] + rho_new[i + 7 + 1][j]);
	}
	spec_thomas_mult_denom( grid + (i + 0)*Mx + 1 , r_y + (My-2)*0 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 1)*Mx + 1 , r_y + (My-2)*1 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 2)*Mx + 1 , r_y + (My-2)*2 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 3)*Mx + 1 , r_y + (My-2)*3 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 4)*Mx + 1 , r_y + (My-2)*4 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 5)*Mx + 1 , r_y + (My-2)*5 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 6)*Mx + 1 , r_y + (My-2)*6 , c_prime_y, c_denom_y, a, b, My-2);
	spec_thomas_mult_denom( grid + (i + 7)*Mx + 1 , r_y + (My-2)*7 , c_prime_y, c_denom_y, a, b, My-2);
}
for (int  i = resy*8 ; i < Mx -1; i++){
	for (int j = 1; j < My - 1; j++) {
                	r_y[j - 1] = -rho_new[i][j] - constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);          
            	}
            	spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);  
           	}
        // ----------------------- STOPHERE1 -------------------------------


        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

        t = t + dt;

    }
    printf("### Simulation Stopped with ADI\n");
    free(r_x);
    free(r_y);
    free(s_x);

    free(c_prime_x);
    free(c_prime_y);
    free(c_denom_x);
    free(c_denom_y);

    free(mem);
}



















































// TODO: include denom
__attribute__((optimize("no-tree-vectorize")))
void adi_solver::solve_scalar_tlp(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    for (int i = 0; i < Mx; i++) {
        for (int j = 0; j < My; j++) {
            rho_new[i][j] = rho[i][j];
        }
    }

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;


    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);

        #pragma omp parallel
        {   
            while (t <= finish) {

                #pragma omp single
                {
                    if (param_vec.save_opt == 1) {
                        copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
                        counter += 1;
                        print_grid_to_file(param_vec.save_file, grid, Mx, t);
                    }
                    else if (param_vec.save_opt == 2){
                        copy_2d(grid, param_vec.trajectory, Mx, My);
                    }

                    #ifdef VERBOSE
                                printf("### Time: %f\n", t);
                    #endif
                }

                #pragma omp single
                {
                    #ifdef TIME_MEASUREMENT
                        if (param_vec.timer_on==true){
                           timer_tic(param_vec.timer);
                        }
                    #endif
                }

                // ------------- Implicit x ----------------------

                
                #pragma omp for
                for (int j = 1; j < My - 1; j++) {
                    double *r_x = (double *) calloc(sizeof(double), Mx - 2);
                    double *s_x = (double *) calloc(sizeof(double), Mx - 2);
                    for (int i = 1; i < Mx - 1; i++) {
                        r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
                    }

                    spec_thomas_mult(s_x, r_x, c_prime_x, a, b, Mx - 2);

                    // copy due to strided access
                    for (int i = 1; i < Mx - 1; i++) {
                        rho_new[i][j] = s_x[i - 1];
                    }
                    free(r_x);
                    free(s_x);
                }
                
                #pragma omp single
                {
                    #ifdef TIME_MEASUREMENT
                        if (param_vec.timer_on==true){
                           timer_toc(param_vec.timer);
                        }
                    #endif
                }

                // ------------- Implicit y --------------------------

                #pragma omp for
                for (int i = 1; i < Mx - 1; i++) {
                    double *r_y = (double *) calloc(sizeof(double), My - 2);
                    for (int j = 1; j < My - 1; j++) {
                        r_y[j - 1] = -rho_new[i][j] -
                                     constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);
                    }
                    spec_thomas_mult(grid + i * Mx + 1, r_y, c_prime_y, a, b, My - 2);
                    // no copy needed as no strided access
                    free(r_y);
                }

                #pragma omp single
                {
                    #ifdef TIME_MEASUREMENT
                        if (param_vec.timer_on==true){
                           timer_toc(param_vec.timer);
                        }
                    #endif
                }

                #pragma omp single
                {
                    t = t + dt;
                }


            }
        }


    printf("### Simulation Stopped with ADI\n");

    free(c_prime_x);
    free(c_prime_y);

    free(mem);
}


__attribute__((optimize("no-tree-vectorize")))
void adi_solver::solve_scalar_unroll_tlp(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    for (int i = 0; i < Mx; i++) {
        for (int j = 0; j < My; j++) {
            rho_new[i][j] = rho[i][j];
        }
    }

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;


    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);

#pragma omp parallel
    {

    while (t <= finish) {

        #pragma omp single
        {

            if (param_vec.save_opt == 1) {
                copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
                counter += 1;
                print_grid_to_file(param_vec.save_file, grid, Mx, t);
            }
            else if (param_vec.save_opt == 2){
                copy_2d(grid, param_vec.trajectory, Mx, My);
            }
            #ifdef VERBOSE
                    printf("### Time: %f\n", t);
            #endif

        }

        #pragma omp single
        {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                   timer_tic(param_vec.timer);
                }
            #endif
        }



        // ----------------------- INSERTHERE2 -------------------------------
#pragma omp for        
for (int j = 1; j < My - 1 - 8; j = j+8) {
    double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*8);
    double *s_x = (double *) calloc(sizeof(double), (Mx - 2)*8);
    for (int i = 1; i < Mx - 1; i++) {
    r_x[(Mx-2)*0+i - 1] = -rho[i][j + 0] - constant * (rho[i][j - 1 + 0] - 2 * rho[i][j + 0] + rho[i][j + 1 + 0]);
    r_x[(Mx-2)*1+i - 1] = -rho[i][j + 1] - constant * (rho[i][j - 1 + 1] - 2 * rho[i][j + 1] + rho[i][j + 1 + 1]);
    r_x[(Mx-2)*2+i - 1] = -rho[i][j + 2] - constant * (rho[i][j - 1 + 2] - 2 * rho[i][j + 2] + rho[i][j + 1 + 2]);
    r_x[(Mx-2)*3+i - 1] = -rho[i][j + 3] - constant * (rho[i][j - 1 + 3] - 2 * rho[i][j + 3] + rho[i][j + 1 + 3]);
    r_x[(Mx-2)*4+i - 1] = -rho[i][j + 4] - constant * (rho[i][j - 1 + 4] - 2 * rho[i][j + 4] + rho[i][j + 1 + 4]);
    r_x[(Mx-2)*5+i - 1] = -rho[i][j + 5] - constant * (rho[i][j - 1 + 5] - 2 * rho[i][j + 5] + rho[i][j + 1 + 5]);
    r_x[(Mx-2)*6+i - 1] = -rho[i][j + 6] - constant * (rho[i][j - 1 + 6] - 2 * rho[i][j + 6] + rho[i][j + 1 + 6]);
    r_x[(Mx-2)*7+i - 1] = -rho[i][j + 7] - constant * (rho[i][j - 1 + 7] - 2 * rho[i][j + 7] + rho[i][j + 1 + 7]);
    }
    spec_thomas_mult_denom( s_x + (Mx-2)*0, r_x + (Mx-2)*0, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*1, r_x + (Mx-2)*1, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*2, r_x + (Mx-2)*2, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*3, r_x + (Mx-2)*3, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*4, r_x + (Mx-2)*4, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*5, r_x + (Mx-2)*5, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*6, r_x + (Mx-2)*6, c_prime_x, c_denom_x, a, b, Mx-2);
    spec_thomas_mult_denom( s_x + (Mx-2)*7, r_x + (Mx-2)*7, c_prime_x, c_denom_x, a, b, Mx-2);
for (int i = 1; i < Mx - 1; i++) {
    rho_new[i][j + 0] = s_x[(Mx-2)*0+i - 1];
    rho_new[i][j + 1] = s_x[(Mx-2)*1+i - 1];
    rho_new[i][j + 2] = s_x[(Mx-2)*2+i - 1];
    rho_new[i][j + 3] = s_x[(Mx-2)*3+i - 1];
    rho_new[i][j + 4] = s_x[(Mx-2)*4+i - 1];
    rho_new[i][j + 5] = s_x[(Mx-2)*5+i - 1];
    rho_new[i][j + 6] = s_x[(Mx-2)*6+i - 1];
    rho_new[i][j + 7] = s_x[(Mx-2)*7+i - 1];
    }
        free(s_x);
        free(r_x);

}

        #pragma omp single
        {
for(int  j = My - 2 - 8; j < My -1; j++){
    double *r_x = (double *) calloc(sizeof(double), (Mx - 2));
    double *s_x = (double *) calloc(sizeof(double), (Mx - 2));
    for (int i = 1; i < Mx - 1; i++) {
                    r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
                }
                spec_thomas_mult_denom( s_x, r_x, c_prime_x, c_denom_x, a, b, Mx-2);
                for(int i = 1; i < Mx - 1; i++) {
                    rho_new[i][j] = s_x[i - 1];    
                } 
        free(s_x);
        free(r_x);
            }
}

                #pragma omp single
                {
                    #ifdef TIME_MEASUREMENT
                        if (param_vec.timer_on==true){
                           timer_toc(param_vec.timer);
                        }
                    #endif
                }

#pragma omp for
for (int i = 1; i < Mx - 1 - 8; i = i+8) {
double *r_y = (double *) calloc(sizeof(double), (My - 2)*8);
    for (int j = 1; j < My - 1; j++) {
        r_y[(My-2)*0+j - 1] = -rho_new[i + 0][j] - constant * (rho_new[i - 1 + 0][j] - 2 * rho_new[i + 0][j] + rho_new[i + 0 + 1][j]);
        r_y[(My-2)*1+j - 1] = -rho_new[i + 1][j] - constant * (rho_new[i - 1 + 1][j] - 2 * rho_new[i + 1][j] + rho_new[i + 1 + 1][j]);
        r_y[(My-2)*2+j - 1] = -rho_new[i + 2][j] - constant * (rho_new[i - 1 + 2][j] - 2 * rho_new[i + 2][j] + rho_new[i + 2 + 1][j]);
        r_y[(My-2)*3+j - 1] = -rho_new[i + 3][j] - constant * (rho_new[i - 1 + 3][j] - 2 * rho_new[i + 3][j] + rho_new[i + 3 + 1][j]);
        r_y[(My-2)*4+j - 1] = -rho_new[i + 4][j] - constant * (rho_new[i - 1 + 4][j] - 2 * rho_new[i + 4][j] + rho_new[i + 4 + 1][j]);
        r_y[(My-2)*5+j - 1] = -rho_new[i + 5][j] - constant * (rho_new[i - 1 + 5][j] - 2 * rho_new[i + 5][j] + rho_new[i + 5 + 1][j]);
        r_y[(My-2)*6+j - 1] = -rho_new[i + 6][j] - constant * (rho_new[i - 1 + 6][j] - 2 * rho_new[i + 6][j] + rho_new[i + 6 + 1][j]);
        r_y[(My-2)*7+j - 1] = -rho_new[i + 7][j] - constant * (rho_new[i - 1 + 7][j] - 2 * rho_new[i + 7][j] + rho_new[i + 7 + 1][j]);
    }
    spec_thomas_mult_denom( grid + (i + 0)*Mx + 1 , r_y + (My-2)*0 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 1)*Mx + 1 , r_y + (My-2)*1 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 2)*Mx + 1 , r_y + (My-2)*2 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 3)*Mx + 1 , r_y + (My-2)*3 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 4)*Mx + 1 , r_y + (My-2)*4 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 5)*Mx + 1 , r_y + (My-2)*5 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 6)*Mx + 1 , r_y + (My-2)*6 , c_prime_y, c_denom_y, a, b, My-2);
    spec_thomas_mult_denom( grid + (i + 7)*Mx + 1 , r_y + (My-2)*7 , c_prime_y, c_denom_y, a, b, My-2);
free(r_y);
}

        #pragma omp single
        {
for (int  i = Mx - 2 - 8; i < Mx -1; i++){
    double *r_y = (double *) calloc(sizeof(double), (My - 2));

    for (int j = 1; j < My - 1; j++) {
                    r_y[j - 1] = -rho_new[i][j] - constant * (rho_new[i - 1][j] - 2 * rho_new[i][j] + rho_new[i + 1][j]);          
                }
                spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);  
                    free(r_y);
            }
        }
        // ----------------------- STOPHERE2 -------------------------------


        #pragma omp single
        {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                   timer_toc(param_vec.timer);
                }
            #endif
        }

        #pragma omp single
        {
        t = t + dt;
        }


    }
}
    printf("### Simulation Stopped with ADI\n");


    free(c_prime_x);
    free(c_prime_y);
    free(c_denom_x);
    free(c_denom_y);

    free(mem);
}



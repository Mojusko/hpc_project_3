
#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "adi_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"
#include <stdio.h>

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 0.1;
const double Dconst = 0.1;

//#define CORRECT


int main( int argc, char *argv[] ) {

    // initialize grid
    int M = 2048; 

        int Mx = M; 
        int My = M; 

        double delta_t = 0.001; 

        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

                double * grid = (double *) calloc(sizeof(double),Mx*My);

                // store trajectories
                double *traj_adi_scalar;
                double *traj_adi_vector;
                double *traj_adi_int;
                double *traj_adi_int_tlp; 
                double *traj_analytic;

                printf("### Allocate memory for simulation.\n");
                int frames = (int)(total_t/delta_t) +1;
                printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
                printf("Numer of frames allocated: %d\n",frames);

                // allocae mem;
                traj_adi_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_int = (double *) calloc(sizeof(double), Mx * My  );
                traj_adi_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

                traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

                // parameter objects
                param params_adi_scalar, params_adi_vector, params_adi_int, params_adi_ispc2, params_adi_int_tlb, params_analytic;
                
                profile_timer_t timer1, timer2, timer3, timer4, timer5; 

                // Alternating Implicit Direction Solver
                params_adi_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_scalar,1,"save_adi_scalar.txt",1) ;
                params_adi_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_vector,1,"save_adi_vector.txt",1) ;
                params_adi_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int,1,"save_adi_int.txt",1) ;
                params_adi_int_tlb.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int_tlp,1,"save_adi_int_tlp.txt",1) ;
                params_adi_ispc2.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int_tlp,1,"save_adi_int_tlp.txt",1) ;


                params_adi_scalar.set_timer(timer1);
                params_adi_vector.set_timer(timer2);
                params_adi_int.set_timer(timer3);
                params_adi_int_tlb.set_timer(timer4);
                params_adi_ispc2.set_timer(timer5);

                // Analytic Solver
                //params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_analytic,1,"save_analytic.txt",1);

                // Solver objects
                adi_solver solver;


                //for (int c = 1; c <=36 ; c++){
                int c = atoi(argv[1]); 
                // Solve
                    double opcount = 24*(M-2)*(M-2);
                    // #pragma omp num_threads(var)
                    omp_set_dynamic(0);     // Explicitly disable dynamic teams
                    omp_set_num_threads(c); // Use 4 threads for all consecutive parallel regions


                    set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                    set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);    

                    solver.solve_vector_ispc2_tlp(grid, params_adi_scalar);

                    
                    double dur = timer_clock_diff_2(params_adi_scalar.timer); 
                    
                    printf("P: c: %2d Grid: %4d \t %20s %4.4f \n",c,M,"Scalar:",dur);
                    printf("P: c: %2d Perf: %4d \t %20s %4.4f \n",c,M,"Scalar:",opcount/(dur*FREQUENCY_COEFFICIENT));

                
          
                // free memmory
                free(traj_adi_scalar);
                free(traj_adi_vector);
                free(traj_adi_int);
                free(traj_adi_int_tlp);
                free(traj_analytic);
                free(grid);
    

    return 0;


}

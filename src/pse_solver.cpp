//
// Created by mojko on 7/26/16.
//

#include "pse_solver.h"
pse_solver::pse_solver() { };
pse_solver::~pse_solver() { };

void pse_solver::solve(double *grid, param & param_vec) {

    int Mx = param_vec.Mx;
    int My = param_vec.My;

    double D = param_vec.D;
    double h = param_vec.space_step;
    double dt = param_vec.time_step;
    int mode = param_vec.mode; 

    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double *init = mem; 

    double finish = param_vec.total_time;
    double t = 0;


    double CFL = D*dt /(h*h);

    if (CFL > 0.5){
        printf("### CFL fail: %4.5f\n",CFL);
        return;
    }
    else{
        printf("### CFL score: %4.5f\n",CFL);
    }


    int counter = 0;
    printf("### Simulation Started with PSE\n");

    #pragma omp parallel
    {
        while (t <= finish) {
            #pragma omp single
            {
                #ifdef VERBOSE
                    printf("### Time: %f\n", t);
                #endif
                if (param_vec.save_opt == 1) {
                    print_grid_to_file(param_vec.save_file, grid, Mx, t);
                    copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
                    counter += 1;
                }
                else if (param_vec.save_opt == 2){
                    copy_2d(grid, param_vec.trajectory, Mx, My);
                }
            }
            

/*            #ifdef OPENMP
                #ifdef ISPC
                    this->step_vector_ispc2_tlp(grid,mem,param_vec);
                #else
                    this->step_scalar_tlp(grid,mem,param_vec);
                #endif
            #else
                #ifdef ISPC
                    this->step_vector_ispc2(grid, mem, param_vec);
                #else
                    this->step_scalar(grid,mem,param_vec);
                #endif
            #endif

*/
            // for testing purposes only
            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                    timer_tic(param_vec.timer);
                }
            #endif
            }


            switch(mode){
                case 1: this->step_scalar(grid,mem,param_vec);
                        break;
                case 2: this->step_scalar_tlp(grid,mem,param_vec);
                        break;
                case 3: this->step_vector_ispc(grid,mem,param_vec);
                        break;
                case 4: this->step_vector_ispc_tlp(grid,mem,param_vec);
                        break;
                case 5: this->step_vector_ispc2(grid,mem,param_vec);
                        break;
                case 6: this->step_vector_ispc2_tlp(grid,mem,param_vec);
                        break;
                case 7: this->step_scalar_imp(grid,mem,param_vec);
                        break;
                case 8: this->step_vector_ispc2_precomp(grid,mem,param_vec);
                        break;

            }


            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                    timer_toc(param_vec.timer);
                }
            #endif
            }


            #pragma omp single
            {
                double *temp = mem;
                mem = grid;
                grid = temp;

                t = t + dt;
            }
        }
    }
    printf("### Simulation Ended with PSE\n");
    free(init);
}


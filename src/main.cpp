#include <iostream>
#include "parameters.h"
#include <functional>
#include <cmath>
#include "param.h"
#include "adi_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"

using namespace std;

/*
 *  Code for HPC project 3, simulation of difussion PDE
 * Diffusion is the process that describes the spreading of a quantity of interest driven by its
concentration gradient towards regions with lower density. In this project we will consider
diffusion in a two-dimensional domain medium that can be described by the diffusion equation
of the form:
                    ∂ρ(r, t) = D∆ρ(r, t)  (1)

where the diffusion coefficient D is a constant and ρ(r, t) is a scalar quantity of interest in
position r at time t. We will use Dirichlet boundary conditions:

                    ρ(0, y, t) = ρ(x, 0, t) = ρ(1, y, t) = ρ(x, 1, t) = 0  ∀t ≥ 0  (2)
and an initial density distribution:
                                ρ(x, y, 0) = sin(πx) · sin(πy) (3)
for which the analytical solution is given by

                    ρ(x, y, t) = sin(πx) · sin(πy) · e−2Dπ^2t  (4)
*/
/*
This code implement four methods to solve the above diffusion equation
                (i) Analytical means
                (ii) ADI solver
                (iii) PSE solver
                (iv)* Random Walks
 */

int main() {

    // initialize grid
    double * grid = (double *) calloc(sizeof(double),Mx*My);

    // store trajectories
    double *traj_adi, *traj_analytic, *traj_pse;
    profile_timer_t timer;
    printf("### Allocate memory for simulation.\n");

    int frames = (int)(total_t/delta_t) +1;


    printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
    printf("Numer of frames allocated: %d\n",frames);

    traj_adi = (double *) calloc(sizeof(double), Mx * My * frames );
    traj_analytic = (double *) calloc(sizeof(double), Mx * My * frames );
    traj_pse = (double *) calloc(sizeof(double), Mx * My * frames );

    param params_adi,params_analytic, params_pse;

    // Alternating Implicit Direction Solver
    params_adi.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,1,traj_adi,1,"save_adi.txt",1) ;

    // Analytic Solver
    params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,1,traj_analytic,1,"save_analytic.txt",1);

    // Particle Strenght Exchange (PSE) solver
    params_pse.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,1,traj_pse,1,"save_pse.txt",8);

    // Solver objects
    adi_solver solver;
    analytic a_solver;
    pse_solver p_solver;

    // Solve
    omp_set_dynamic(0);     // Explicitly disable dynamic teams
    omp_set_num_threads(4); // Use 4 threads for all consecutive parallel regions


    set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    solver.solve_scalar_unroll(grid, params_adi);

    //solver.solve_scalar(grid, params_adi);
    /*set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    p_solver.solve(grid, params_pse);
    */
    
/*    timer_reset(timer);
    set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);

    timer_tic(timer);
    p_solver.solve(grid, params_pse);
    timer_toc(timer);
    cout << "Time: " << timer_clock_diff(timer) << " ns" << endl;

*/
    set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
    a_solver.evaluate_trajectory(grid,params_analytic);

   /* 
     set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
     set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);

    //p_solver.solve(grid, params_pse);
    */
    // compare the strajectories
    compare_2d(params_adi.trajectory,params_analytic.trajectory,params_adi.Mx,params_adi.My,(int)(params_adi.total_time/params_adi.time_step));
    //compare_2d(params_pse.trajectory,params_analytic.trajectory,params_adi.Mx,params_adi.My,(int)(params_adi.total_time/params_adi.time_step));


    // free memmory
    free(traj_adi);
    free(traj_analytic);
    free(traj_pse);
    free(grid);


    return 0;


}

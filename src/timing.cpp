//
// Created by mojko on 7/29/16.
//

#include <timing.h>

void timer_tic(profile_timer_t & timer){
    timer.start = std::chrono::steady_clock::now();
    timer.count = 0; 
    timer.runs = timer.runs + 1; 
}

void timer_toc(profile_timer_t & timer){
    timer.end = std::chrono::steady_clock::now();
    //timer.diff[timer.count] = std::chrono::duration <double, std::micro> (timer.end - timer.start).count();
    timer.diff[timer.count] = ((timer.diff[timer.count]*(double)timer.runs) + std::chrono::duration <double, std::micro> (timer.end - timer.start).count())/((double)timer.runs+1.);
    timer.count++;
}

void timer_reset(profile_timer_t &  timer){
    timer.diff[0] = 0.;
    timer.diff[1] = 0.;
    timer.runs = -1;
}

double timer_clock_diff(profile_timer_t & timer){
    return timer.diff[0];
}

double timer_clock_diff_2(profile_timer_t & timer){
    return timer.diff[1];
}
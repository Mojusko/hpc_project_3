#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "pse_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 1;
const double Dconst = 0.001;

//#define CORRECT


int main() {

    // initialize grid
    int M = 32; 
    /*for (int M = 16; M<= 256; M=M*2)
    {*/
    for (int T = 2; T<=pow(2,12); T=T*2 ){
        int Mx = M; 
        int My = M; 


        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

        //double delta_t = (0.5/Dconst)*delta_x*delta_x; 
        double delta_t = total_t/T;

                double * grid = (double *) calloc(sizeof(double),Mx*My);

                // store trajectories
                double *traj_pse_scalar;
                double *traj_pse_vector;
                double *traj_pse_int;
                double *traj_pse_int_tlp; 
                double *traj_analytic;

                printf("### Allocate memory for simulation.\n");
                int frames = (int)(total_t/delta_t) +1;
                printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
                printf("Numer of frames allocated: %d\n",frames);

                // allocae mem;
                traj_pse_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_pse_vector = (double *) calloc(sizeof(double), Mx * My );
                traj_pse_int = (double *) calloc(sizeof(double), Mx * My  );
                traj_pse_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

                traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

                // parameter objects
                param params_pse_scalar, params_pse_vector, params_pse_int, params_pse_int_tlb, params_analytic;

                // Alternating Implicit Direction Solver
                params_pse_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_pse_scalar,1,"save_pse_scalar.txt",1) ;
                params_pse_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_pse_vector,1,"save_pse_vector.txt",3) ;
                params_pse_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_pse_int,1,"save_pse_int.txt",5) ;
                params_pse_int_tlb.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_pse_int_tlp,1,"save_pse_int_tlp.txt",6) ;

                // Analytic Solver
                params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_analytic,1,"save_analytic.txt",1);


                // Solver objects
                pse_solver solver;
                analytic a_solver;

                // Solve
                

/*                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve(grid, params_pse_scalar);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve(grid, params_pse_vector);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve(grid, params_pse_int);
*/
                

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve(grid, params_pse_int_tlb);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                a_solver.evaluate_trajectory(grid,params_analytic);

                //printf("%20s%10s \t %10s \t %10s \n","T:","step size","time step","Error");

                #ifdef CORRECT
                printf("%20s\t","T: Scalar:");
                test_max_norm(params_pse_scalar.trajectory,params_analytic.trajectory,params_pse_scalar.Mx,params_pse_scalar.My,delta_t,delta_x,frames);
                printf("%20s\t","T: ISPC:");
                test_max_norm(params_pse_vector.trajectory,params_analytic.trajectory,params_pse_scalar.Mx,params_pse_scalar.My,delta_t,delta_x,frames);
                printf("%20s\t","T: ISPC2:");
                test_max_norm(params_pse_int.trajectory,params_analytic.trajectory,params_pse_scalar.Mx,params_pse_scalar.My,delta_t,delta_x,frames);
                #endif

                printf("%20s\t","T: ISPC2_TLP:");
                test_max_norm(params_pse_int_tlb.trajectory,params_analytic.trajectory,params_pse_scalar.Mx,params_pse_scalar.My,delta_x,delta_t,frames);

                // free memmory
                free(traj_pse_scalar);
                free(traj_pse_vector);
                free(traj_pse_int);
                free(traj_pse_int_tlp);
                free(traj_analytic);
                free(grid);
    }

    return 0;


}
#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "adi_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 1;
const double Dconst = 0.01;

//#define CORRECT


int main() {

    // initialize grid
    for (int M = 16; M<= 2048; M=M*2)
    {
        int Mx = M; 
        int My = M; 
        double delta_t = 0.01; 

        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

        //while (delta_t >= 0.001) {
                double * grid = (double *) calloc(sizeof(double),Mx*My);

                // store trajectories
                double *traj_adi_scalar;
                double *traj_adi_vector;
                double *traj_adi_int;
                double *traj_adi_int_tlp; 
                double *traj_analytic;

                printf("### Allocate memory for simulation.\n");
                int frames = (int)(total_t/delta_t) +1;
                printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
                printf("Numer of frames allocated: %d\n",frames);

                //delta_t = (0.5)*delta_x*delta_x; 

                //double CLF = delta_t/(delta_x*delta_x);
            /*    if (CLF > 1./2.){
                    printf("CLF Fail score: %4.4f\n",CLF);
                   // return 0;
                }*/

                // allocae mem;
                traj_adi_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_vector = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_int = (double *) calloc(sizeof(double), Mx * My  );
                traj_adi_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

                traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

                // parameter objects
                param params_adi_scalar, params_adi_vector, params_adi_int, params_adi_int_tlb, params_analytic;

                // Alternating Implicit Direction Solver
                params_adi_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_adi_scalar,1,"save_adi_scalar.txt",1) ;
                params_adi_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_adi_vector,1,"save_adi_vector.txt",1) ;
                params_adi_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_adi_int,1,"save_adi_int.txt",1) ;
                params_adi_int_tlb.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_adi_int_tlp,1,"save_adi_int_tlp.txt",1) ;

                // Analytic Solver
                params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_analytic,1,"save_analytic.txt",1);


                // Solver objects
                adi_solver solver;
                analytic a_solver;

                // Solve
                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_scalar(grid, params_adi_scalar);

/*                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_ispc(grid, params_adi_vector);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_int(grid, params_adi_int);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_int_tlp(grid, params_adi_int_tlb);
                */
                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                a_solver.evaluate_trajectory(grid,params_analytic);

                //printf("%20s%10s \t %10s \t %10s \n","T:","step size","time step","Error");

                printf("%20s\t","T: Scalar:");
                test_max_norm(params_adi_scalar.trajectory,params_analytic.trajectory,params_adi_scalar.Mx,params_adi_scalar.My,delta_t,delta_x,frames);
                // free memmory
                free(traj_adi_scalar);
                free(traj_adi_vector);
                free(traj_adi_int);
                free(traj_adi_int_tlp);
                free(traj_analytic);
                free(grid);
        //delta_t/=10;
        //}
    }

    return 0;


}
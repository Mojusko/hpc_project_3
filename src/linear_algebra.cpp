//
// Created by mojko on 7/26/16.
//

#include "linear_algebra.h"


//  Special thomas algorithm preparation step for the case when banded matrix is constant allong band

void spec_thomas_invert(double a, double b, double c, double *c_prime, int n){
    c_prime[0] = c/b;
    for (int i = 1; i<n-1; i++){
        c_prime[i] =  c/(b - a*c_prime[i-1]);
    }
}

void spec_thomas_denom(double a, double b, double *c_prime, double *denom, int n){
    for (int i = 0; i<n-1; i++){
        denom[i] =  1./(b - a*c_prime[i]);
    }
}
/*
void thomas_solve(Eigen::MatrixXd & A, Eigen::VectorXd & d, Eigen::VectorXd & x){
    int n = (int) d.size();
    Eigen::VectorXd b, a, c, c_prime;

    b = A.diagonal();
    c = A.diagonal(1);
    a = A.diagonal(-1);

    c_prime = Eigen::VectorXd::Zero(n-1);

    c_prime(0) = c(0)/b(0);
    d(0) = d(0)/b(0);


    for (int i = 1; i<n-1; i++){
        c_prime(i) =  c(i)/(b(i) - a(i-1)*c_prime(i-1));
        d(i) =  (d(i) - a(i-1)*d(i-1))/(b(i)- a(i-1)*c_prime(i-1));
    }
    d(n-1) = (d(n-1) - a(n-2)*d(n-2))/(b(n-1) - a(n-2)*c_prime(n-2));

    x(n-1)=d(n-1);
    for (int i = n-2; i>=0 ; i--){
        x(i) = d(i)-c_prime(i)*x(i+1);
    }
};*/
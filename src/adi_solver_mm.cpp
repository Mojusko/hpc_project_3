#include "adi_solver.h"


void adi_solver::solve_vector_int(double *grid, param  & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;
    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;     

    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    const double constant = (D * dt / (2 * h * h));

    // allocate mmemory
    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);

    double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*4 );
    double *s_x = (double *) calloc(sizeof(double), (Mx - 2) );
    double *r_y = (double *) calloc(sizeof(double), (My - 2)*UNROLL);

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);


    // vector constants
    const __m256d const_v = _mm256_set1_pd(constant);
    const __m256d minus_2_v = _mm256_set1_pd(-2.0);
    const __m256d minus_1_v = _mm256_set1_pd(-1.0);


    while (t <= finish) {

        if (param_vec.save_opt == 1) {
            copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
            counter += 1;
            print_grid_to_file(param_vec.save_file, grid, Mx, t);
        }
        else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
        }
        #ifdef VERBOSE
                printf("### Time: %f\n", t);
        #endif


        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_tic(param_vec.timer);
            }
        #endif

        // ------------- Implicit x ----------------------
        for (int j = 0; j < My - 8 ; j = j + 4) {

            for (int i = 1; i < Mx-1-4 ; i = i + 4) {

                //UNROLL THIS LOOP COMPLETELY

                __m256d v1 = _mm256_loadu_pd(&rho[i][j]);
                __m256d v2 = _mm256_loadu_pd(&rho[i+1][j]);
                __m256d v3 = _mm256_loadu_pd(&rho[i+2][j]);
                __m256d v4 = _mm256_loadu_pd(&rho[i+3][j]);

                __m256d v1_2 = _mm256_loadu_pd(&rho[i][j+4]);
                __m256d v2_2 = _mm256_loadu_pd(&rho[i+1][j+4]);
                __m256d v3_2 = _mm256_loadu_pd(&rho[i+2][j+4]);
                __m256d v4_2 = _mm256_loadu_pd(&rho[i+3][j+4]);

                __m256d tmp3, tmp2, tmp1, tmp0;                              
                                                                             
                tmp0 = _mm256_shuffle_pd((v1),(v2), 0x0);                    
                tmp2 = _mm256_shuffle_pd((v1),(v2), 0xF);                
                tmp1 = _mm256_shuffle_pd((v3),(v4), 0x0);                    
                tmp3 = _mm256_shuffle_pd((v3),(v4), 0xF);                
                                                                             
                v1 = _mm256_permute2f128_pd(tmp0, tmp1, 0x20);   
                v2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x20);   
                v3 = _mm256_permute2f128_pd(tmp0, tmp1, 0x31);   
                v4 = _mm256_permute2f128_pd(tmp2, tmp3, 0x31);   

                tmp0 = _mm256_shuffle_pd((v1_2),(v2_2), 0x0);                    
                tmp2 = _mm256_shuffle_pd((v1_2),(v2_2), 0xF);                
                tmp1 = _mm256_shuffle_pd((v3_2),(v4_2), 0x0);                    
                tmp3 = _mm256_shuffle_pd((v3_2),(v4_2), 0xF);                
                                                                             
                v1_2 = _mm256_permute2f128_pd(tmp0, tmp1, 0x20);   
                v2_2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x20);   
                v3_2 = _mm256_permute2f128_pd(tmp0, tmp1, 0x31);   
                v4_2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x31);   


                // kernels 
                const __m256d sum1 = _mm256_add_pd(v1,v3);
                const __m256d sum11 = _mm256_add_pd(sum1, _mm256_mul_pd(minus_2_v,v2));
                const __m256d r1 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v2,_mm256_mul_pd(const_v,sum11)));

                const __m256d sum2 = _mm256_add_pd(v2,v4);
                const __m256d sum22 = _mm256_add_pd(sum2, _mm256_mul_pd(minus_2_v,v3));
                const __m256d r2 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v3,_mm256_mul_pd(const_v,sum22)));

                const __m256d sum3 = _mm256_add_pd(v3,v1_2);
                const __m256d sum33 = _mm256_add_pd(sum3, _mm256_mul_pd(minus_2_v,v4));
                const __m256d r3 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v4,_mm256_mul_pd(const_v,sum33)));

                const __m256d sum4 = _mm256_add_pd(v4,v2_2);
                const __m256d sum44 = _mm256_add_pd(sum4, _mm256_mul_pd(minus_2_v,v1_2));
                const __m256d r4 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v1_2,_mm256_mul_pd(const_v,sum44)));


                _mm256_storeu_pd(&r_x[(Mx-2)*0 + (i-1)],r1);
                _mm256_storeu_pd(&r_x[(Mx-2)*1 + (i-1)],r2);
                _mm256_storeu_pd(&r_x[(Mx-2)*2 + (i-1)],r3);
                _mm256_storeu_pd(&r_x[(Mx-2)*3 + (i-1)],r4);      
            }


            for (int k = 0; k < 4; k++){
                for (int i = Mx - 6 ; i < Mx - 1; i = i + 1) {
                    r_x[(Mx-2)*k + i - 1] = -rho[i][j+k+1] - constant * (rho[i][j - 1+k+1] - 2 * rho[i][j+k+1] + rho[i][j + 1+k+1]);
                }
            }

            // Solve Thomas 4 times
            for (int q = j + 1 ; q < std::min(5+j,My -1) ; q++){

                // Thomas inlined 
                r_x[(Mx-2)*(q-j-1)+0] = r_x[(Mx-2)*(q-j-1)+0]/b;

                for (int i = 1; i<Mx-2; i++){
                    r_x[(Mx-2)*(q-j-1)+i] =  (r_x[(Mx-2)*(q-j-1) + i] - a*r_x[(Mx-2)*(q-j-1)+i-1])*(c_denom_x[i-1]);
                }

                rho_new[Mx-2][q] = r_x[(Mx-2)*(q-j-1)+Mx-3];

                for (int i = Mx-4; i>=0 ; i--){
                    rho_new[i+1][q] = r_x[(Mx-2)*(q-j-1) + i]- c_prime_x[i]*rho_new[i+2][q];
                }
            }
        }



        for (int j =  My - 8 - 1; j < My - 1; j++) {
            for (int i = 1; i < Mx - 1; i++) {
                r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
            }

            r_x[0] = r_x[0]/b;

            for (int i = 1; i<Mx-2; i++){
                r_x[i] =  (r_x[i] - a*r_x[i-1])*(c_denom_x[i-1]);
            }

            rho_new[Mx-2][j] = r_x[Mx-3];

            for (int i = Mx-4; i>=0 ; i--){
                rho_new[i+1][j] = r_x[i]- c_prime_x[i]*rho_new[i+2][j];
            }

        }   

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

        // ------------- Implicit y --------------------------
        // good access 
        const int steps = (Mx -2)/UNROLL; 

        for (int i = 1; i < steps*UNROLL + 1 ; i = i + UNROLL) {
            
            ispc::adi_full_2(Mx,My,constant,mem + i*My,  r_y);

            for (int k = i; k< i + UNROLL; k++){
                spec_thomas_mult_denom( grid + k*Mx + 1 , r_y + (My-2)*(k-i), c_prime_y, c_denom_y, a, b, My-2);
            }
        }

        for (int i = steps*UNROLL; i < Mx -1; i++){
            ispc::adi_stencil_2(Mx,My,constant, mem + i*My, r_y);
            spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);
        }

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
                timer_toc(param_vec.timer);
            }
        #endif
        t = t + dt;

    }

    printf("### Simulation Stopped with ADI\n");

    free(r_y);
    free(s_x);
    free(r_x);
    free(c_denom_x);
    free(c_denom_y);
    free(c_prime_x);
    free(c_prime_y);

    free(mem);
}




































void adi_solver::solve_vector_int_tlp(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    double (*rho)[My] = (double (*)[My]) grid;
    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;     

    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    const double constant = (D * dt / (2 * h * h));

    // allocate mmemory
    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);


    // vector constants
    const __m256d const_v = _mm256_set1_pd(constant);
    const __m256d minus_2_v = _mm256_set1_pd(-2.0);
    const __m256d minus_1_v = _mm256_set1_pd(-1.0);

    #pragma omp parallel
    {

        while (t <= finish) {

            #pragma omp single
            {
                if (param_vec.save_opt == 1) {
                    copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
                    counter += 1;
                    print_grid_to_file(param_vec.save_file, grid, Mx, t);
                }
                else if (param_vec.save_opt == 2){
                    copy_2d(grid, param_vec.trajectory, Mx, My);
                }                
                #ifdef VERBOSE
                        printf("### Time: %f\n", t);
                #endif
            }


            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                    timer_tic(param_vec.timer);
                }
            #endif
            }



            // ------------- Implicit x ----------------------
            #pragma omp for
            for (int j = 0; j < My - 8 ; j = j + 4) {
                double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*4 );
                for (int i = 1; i < Mx-1-4 ; i = i + 4) {

                    //UNROLL THIS LOOP COMPLETELY

                    __m256d v1 = _mm256_loadu_pd(&rho[i][j]);
                    __m256d v2 = _mm256_loadu_pd(&rho[i+1][j]);
                    __m256d v3 = _mm256_loadu_pd(&rho[i+2][j]);
                    __m256d v4 = _mm256_loadu_pd(&rho[i+3][j]);

                    __m256d v1_2 = _mm256_loadu_pd(&rho[i][j+4]);
                    __m256d v2_2 = _mm256_loadu_pd(&rho[i+1][j+4]);
                    __m256d v3_2 = _mm256_loadu_pd(&rho[i+2][j+4]);
                    __m256d v4_2 = _mm256_loadu_pd(&rho[i+3][j+4]);

                    __m256d tmp3, tmp2, tmp1, tmp0;                              
                                                                                 
                    tmp0 = _mm256_shuffle_pd((v1),(v2), 0x0);                    
                    tmp2 = _mm256_shuffle_pd((v1),(v2), 0xF);                
                    tmp1 = _mm256_shuffle_pd((v3),(v4), 0x0);                    
                    tmp3 = _mm256_shuffle_pd((v3),(v4), 0xF);                
                                                                                 
                    v1 = _mm256_permute2f128_pd(tmp0, tmp1, 0x20);   
                    v2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x20);   
                    v3 = _mm256_permute2f128_pd(tmp0, tmp1, 0x31);   
                    v4 = _mm256_permute2f128_pd(tmp2, tmp3, 0x31);   

                    tmp0 = _mm256_shuffle_pd((v1_2),(v2_2), 0x0);                    
                    tmp2 = _mm256_shuffle_pd((v1_2),(v2_2), 0xF);                
                    tmp1 = _mm256_shuffle_pd((v3_2),(v4_2), 0x0);                    
                    tmp3 = _mm256_shuffle_pd((v3_2),(v4_2), 0xF);                
                                                                                 
                    v1_2 = _mm256_permute2f128_pd(tmp0, tmp1, 0x20);   
                    v2_2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x20);   
                    v3_2 = _mm256_permute2f128_pd(tmp0, tmp1, 0x31);   
                    v4_2 = _mm256_permute2f128_pd(tmp2, tmp3, 0x31);   


                    // kernels 
                    const __m256d sum1 = _mm256_add_pd(v1,v3);
                    const __m256d sum11 = _mm256_add_pd(sum1, _mm256_mul_pd(minus_2_v,v2));
                    const __m256d r1 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v2,_mm256_mul_pd(const_v,sum11)));

                    const __m256d sum2 = _mm256_add_pd(v2,v4);
                    const __m256d sum22 = _mm256_add_pd(sum2, _mm256_mul_pd(minus_2_v,v3));
                    const __m256d r2 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v3,_mm256_mul_pd(const_v,sum22)));

                    const __m256d sum3 = _mm256_add_pd(v3,v1_2);
                    const __m256d sum33 = _mm256_add_pd(sum3, _mm256_mul_pd(minus_2_v,v4));
                    const __m256d r3 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v4,_mm256_mul_pd(const_v,sum33)));

                    const __m256d sum4 = _mm256_add_pd(v4,v2_2);
                    const __m256d sum44 = _mm256_add_pd(sum4, _mm256_mul_pd(minus_2_v,v1_2));
                    const __m256d r4 = _mm256_mul_pd(minus_1_v,_mm256_add_pd(v1_2,_mm256_mul_pd(const_v,sum44)));


                    _mm256_storeu_pd(&r_x[(Mx-2)*0 + (i-1)],r1);
                    _mm256_storeu_pd(&r_x[(Mx-2)*1 + (i-1)],r2);
                    _mm256_storeu_pd(&r_x[(Mx-2)*2 + (i-1)],r3);
                    _mm256_storeu_pd(&r_x[(Mx-2)*3 + (i-1)],r4);      
                }


                for (int k = 0; k < 4; k++){
                    for (int i = Mx - 6 ; i < Mx - 1; i = i + 1) {
                        r_x[(Mx-2)*k + i - 1] = -rho[i][j+k+1] - constant * (rho[i][j - 1+k+1] - 2 * rho[i][j+k+1] + rho[i][j + 1+k+1]);
                    }
                }

                // Solve Thomas 4 times
                for (int q = j + 1 ; q < std::min(5+j,My -1) ; q++){

                    // Thomas inlined 
                    r_x[(Mx-2)*(q-j-1)+0] = r_x[(Mx-2)*(q-j-1)+0]/b;

                    for (int i = 1; i<Mx-2; i++){
                        r_x[(Mx-2)*(q-j-1)+i] =  (r_x[(Mx-2)*(q-j-1) + i] - a*r_x[(Mx-2)*(q-j-1)+i-1])*(c_denom_x[i-1]);
                    }

                    rho_new[Mx-2][q] = r_x[(Mx-2)*(q-j-1)+Mx-3];

                    for (int i = Mx-4; i>=0 ; i--){
                        rho_new[i+1][q] = r_x[(Mx-2)*(q-j-1) + i]- c_prime_x[i]*rho_new[i+2][q];
                    }
                }
                free(r_x);
            }




            #pragma omp single
            {
                double *r_x = (double *) calloc(sizeof(double), (Mx - 2));
                for (int j =  My - 8 - 1; j < My - 1; j++) {
                    for (int i = 1; i < Mx - 1; i++) {
                        r_x[i - 1] = -rho[i][j] - constant * (rho[i][j - 1] - 2 * rho[i][j] + rho[i][j + 1]);
                    }

                    r_x[0] = r_x[0]/b;

                    for (int i = 1; i<Mx-2; i++){
                        r_x[i] =  (r_x[i] - a*r_x[i-1])*(c_denom_x[i-1]);
                    }

                    rho_new[Mx-2][j] = r_x[Mx-3];

                    for (int i = Mx-4; i>=0 ; i--){
                        rho_new[i+1][j] = r_x[i]- c_prime_x[i]*rho_new[i+2][j];
                    }
                }   
                free(r_x);
            }

            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                    timer_toc(param_vec.timer);
                }
            #endif
            }



            // ------------- Implicit y --------------------------
            #pragma omp for
            for (int i = 1; i < ((Mx - 1)/UNROLL)*UNROLL; i = i + UNROLL) {
                double *r_y = (double *) calloc(sizeof(double), (My - 2)*UNROLL);
                ispc::adi_full_2(Mx,My,constant,  mem + i*My,r_y);

                for (int k = i; k< i + UNROLL; k++){
                    spec_thomas_mult_denom( grid + k*Mx + 1 , r_y + (My-2)*(k-i), c_prime_y, c_denom_y, a, b, My-2);
                }
                free(r_y);
                // no copy needed as no strided access
            }

            #pragma omp single
            {
                double *r_y = (double *) calloc(sizeof(double), (My - 2));
                for (int i = ((Mx - 1)/UNROLL)*UNROLL; i < Mx -1; i++){
                    ispc::adi_stencil_2(Mx,My,constant,  mem + i*My,r_y);
                    spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);
                }
                free(r_y);
            }
            

            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                    timer_toc(param_vec.timer);
                }
            #endif
            }


            #pragma omp single
            {   
                t = t + dt;
            }
        }
    }
    printf("### Simulation Stopped with ADI\n");

    free(c_prime_x);
    free(c_prime_y);
    free(c_denom_x);
    free(c_denom_y);

    free(mem);
}

//
// Created by mojko on 7/22/16.
//

#include "param.h"
#include <iostream>
param::param(){

};
param::~param(){

};

void param::set_params(double diff_const, double x_s, double y_s, int grid_size_x, int grid_size_y, double space_step , double time_step, double time, int save_opt, double * traj, int print_opt, std::string save_file, int mode){

    this->timer = timer; 
    this->D = diff_const;
    this->Mx = grid_size_x;
    this->My = grid_size_y;

    this->mode = mode; 
    
    this->x_start = x_s;
    this->y_start = y_s;

    this->space_step = space_step;
    this->time_step = time_step;

    this->save_opt = save_opt;
    this->print_opt = print_opt;

    this->total_time = time;
    this->save_file = save_file;

    printf("### Param object created.\n");

    FILE *f;
    if (save_opt == 1) {
        f = fopen(save_file.c_str(), "r");
        if (f != NULL) {
            // file exists
            fclose(f);
            // make it blank
            f = fopen(save_file.c_str(), "w");
            fclose(f);
        }
    }
    this->trajectory = traj;

};

void param::set_timer(profile_timer_t & timer){
    this->timer = timer; 
    this->timer_on = true;
}


void param::dealloc(){
   /* if (this->save_opt == 1){
        free(this->trajectory);
        printf("### Deallocated memory for simulation.\n");
    }*/
};


#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "pse_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 0.00000000001; 
const double Dconst = 0.1;

//#define CORRECT


int main() {

    // initialize grid
    for (int M = 1024; M<= pow(2,12); M=M*2)
    {
        int Mx = M; 
        int My = M; 

        double delta_t = 0.00000000001; 

        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

                double * grid = (double *) calloc(sizeof(double),Mx*My);

                // store trajectories
                double *traj_pse_scalar;
                double *traj_pse_vector;
                double *traj_pse_int;
                double *traj_pse_int_tlp; 
                double *traj_analytic;

                printf("### Allocate memory for simulation.\n");
                int frames = (int)(total_t/delta_t) +1;
                printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
                printf("Numer of frames allocated: %d\n",frames);

                // allocae mem;
                traj_pse_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_pse_vector = (double *) calloc(sizeof(double), Mx * My );
                traj_pse_int = (double *) calloc(sizeof(double), Mx * My  );
                traj_pse_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

                traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

                // parameter objects
                param params_pse_scalar, params_pse_vector, params_pse_int, params_pse_ispc2, params_pse_int_tlb, params_analytic;
                
                profile_timer_t timer1, timer2, timer3, timer4, timer5; 

                // Alternating Implicit Direction Solver
                params_pse_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_scalar,1,"save_pse_scalar.txt",2) ;
                params_pse_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_vector,1,"save_pse_vector.txt",4) ;
                params_pse_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_int,1,"save_pse_int.txt",6) ;


                params_pse_scalar.set_timer(timer1);
                params_pse_vector.set_timer(timer2);
                params_pse_int.set_timer(timer3);


                // Analytic Solver
                //params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_analytic,1,"save_analytic.txt",1);

                // Solver objects
                pse_solver solver;


                for (int c = 1; c <=36 ; c++){
                // Solve
                    double opcount = (M-2.0)*(1 + (M-2.)*( 1 + (2*SIZE+1)*(   1. + (2*SIZE +1)*(  16 ) + 2 )  )   );
					
					omp_set_dynamic(0);     // Explicitly disable dynamic teams
					omp_set_num_threads(c); // Use 4 threads for all consecutive parallel regions


	                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
	                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);    

	                solver.solve(grid, params_pse_int);
	                
	                double dur = timer_clock_diff(params_pse_int.timer); 
	                timer_reset(params_pse_int.timer);

	                printf("P: c: %2d Grid: %4d \t %20s %4.4f \n",c,M,"ISPC:",dur);
	                printf("P: c: %2d Perf: %4d \t %20s %4.4f \n",c,M,"ISPC:",opcount/(dur*FREQUENCY_COEFFICIENT));

            	}
          
                // free memmory
                free(traj_pse_scalar);
                free(traj_pse_vector);
                free(traj_pse_int);
                free(traj_pse_int_tlp);
                free(traj_analytic);
                free(grid);
    }

    return 0;


}
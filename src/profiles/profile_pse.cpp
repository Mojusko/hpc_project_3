#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "adi_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"
#include "omp.h"

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 0.00000001; 
const double Dconst = 0.01;

//#define CORRECT


int main() {

    // initialize grid
    for (int M = 16; M<= pow(2,14); M=M*2)
    {
        int Mx = M; 
        int My = M; 

        double delta_t = 0.0000001; 

        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

            double * grid;// = (double *) calloc(sizeof(double),Mx*My);
            posix_memalign((void **) &grid,  4*sizeof(double),   Mx * My * sizeof(double));

            // store trajectories
            double *traj_pse_scalar;
            double *traj_pse_vector;
            double *traj_pse_int;
            double *traj_pse_int_tlp; 
            double *traj_analytic;

            printf("### Allocate memory for simulation.\n");
            int frames = (int)(total_t/delta_t) +1;
            printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
            printf("Numer of frames allocated: %d\n",frames);

            // allocae mem;
            traj_pse_scalar = (double *) calloc(sizeof(double), Mx * My );
            traj_pse_vector = (double *) calloc(sizeof(double), Mx * My );
            traj_pse_int = (double *) calloc(sizeof(double), Mx * My  );
            traj_pse_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

            traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

            // parameter objects
            param params_pse_scalar, params_pse_vector, params_pse_int, params_pse_imp, params_analytic;
            
            profile_timer_t timer1, timer2, timer3, timer4; 

            // Alternating Implicit Direction Solver
            params_pse_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_scalar,1,"save_pse_scalar.txt",1) ;
            params_pse_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_vector,1,"save_pse_vector.txt",3) ;
            params_pse_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_int,1,"save_pse_int.txt",5) ;
            params_pse_imp.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_pse_int,1,"save_pse_int.txt",8) ;

            params_pse_scalar.set_timer(timer1);
            params_pse_vector.set_timer(timer2);
            params_pse_int.set_timer(timer3);
            params_pse_imp.set_timer(timer4);

            // Solver objects
            pse_solver solver;
            omp_set_dynamic(0);     // Explicitly disable dynamic teams
            omp_set_num_threads(1); // Use 4 threads for all consecutive parallel regions

            // Solve
            double opcount = (M-2.0)*(1 + (M-2.)*( 1 + (2*SIZE+1)*(   1. + (2*SIZE +1)*(  16 ) + 2 )  )   );

            set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);                
            solver.solve(grid, params_pse_scalar);
            double dur = timer_clock_diff(params_pse_scalar.timer); 
            double dur_scalar = dur;
            printf("P: Grid: %4d \t %20s %4.4f %4.4f \n",M,"Scalar:",dur,dur_scalar);
            printf("P: Perf: %4d \t %20s %4.4f \n",M,"Scalar:",opcount/(dur*FREQUENCY_COEFFICIENT));
            timer_reset(params_pse_scalar.timer);

/*            set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            solver.solve(grid, params_pse_vector);
            dur = timer_clock_diff(params_pse_vector.timer); 
            printf("P: Grid: %4d \t %20s %4.4f \n",M,"ISPC:",dur);
            printf("P: Perf: %4d \t %20s %4.4f\n",M,"ISPC:",opcount/(dur*FREQUENCY_COEFFICIENT));
*/
            set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            solver.solve(grid, params_pse_int);
            dur = timer_clock_diff(params_pse_int.timer); 
            printf("P: Grid: %4d \t %20s %4.4f %4.4f \n",M,"ISPC2:",dur,dur_scalar);
            printf("P: Perf: %4d \t %20s %4.4f \n",M,"ISPC2:",opcount/(dur*FREQUENCY_COEFFICIENT));
            timer_reset(params_pse_int.timer);


            set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            solver.solve(grid, params_pse_imp);
            dur = timer_clock_diff(params_pse_imp.timer); 
            printf("P: Grid: %4d \t %20s %4.4f %4.4f \n",M,"PRECOMPISPC:",dur,dur_scalar);
            double opcount2 = (M-2.0)*( (M-2.)*(  (2*SIZE+1)*(    (2*SIZE +1)*(  5 ) + 2 )  )   );
            printf("P: Perf: %4d \t %20s %4.4f \n",M,"PRECOMPISPC:",opcount2/(dur*FREQUENCY_COEFFICIENT));
            timer_reset(params_pse_imp.timer);

/*
            set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
            solver.solve(grid, params_pse_imp);
            dur = timer_clock_diff(params_pse_imp.timer); 
            printf("P: Grid: %4d \t %20s %4.4f \n",M,"IMP:",dur);
            printf("P: Perf: %4d \t %20s %4.4f \n",M,"IMP:",opcount/(dur*FREQUENCY_COEFFICIENT));
            */
            // free memmory
            free(traj_pse_scalar);
            free(traj_pse_vector);
            free(traj_pse_int);
            free(traj_pse_int_tlp);
            free(traj_analytic);
            free(grid);
    }

    return 0;


}

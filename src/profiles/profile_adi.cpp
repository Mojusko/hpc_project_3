#include <iostream>
#include <functional>
#include <cmath>
#include "param.h"
#include "adi_solver.h"
#include "helper.h"
#include "analytic.h"
#include "pse_solver.h"
#include "meta_params.h"
#include "timing.h"

using namespace std;

// rectangular domain only
const double xstart  = 0.0;
const double xend  = 1.0;

const double ystart  = 0.0;
const double yend  = 1.0;

const double xext = xend - xstart;
const double yext = yend - ystart;


const double total_t = 0.1;
const double Dconst = 0.1;

//#define CORRECT


int main() {

    // initialize grid
    for (int M = 32; M<= pow(2,14); M=M*2)
    {
        int Mx = M; 
        int My = M; 

        double delta_t = 0.01; 

        const double delta_x = 1./(Mx - 1);
        const double delta_y = 1./(My - 1);

                double * grid; //= (double *) calloc(sizeof(double),Mx*My);
                posix_memalign((void **) &grid,  4*sizeof(double),   Mx * My * sizeof(double));

                // store trajectories
                double *traj_adi_scalar;
                double *traj_adi_vector;
                double *traj_adi_int;
                double *traj_adi_int_tlp; 
                double *traj_analytic;

                printf("### Allocate memory for simulation.\n");
                int frames = (int)(total_t/delta_t) +1;
                printf("Grid %d x %d, with time step: %3.3f, simulating from [0,%3.3f]\n",Mx,My,delta_t,total_t);
                printf("Numer of frames allocated: %d\n",frames);

                // allocae mem;
                traj_adi_scalar = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_vector = (double *) calloc(sizeof(double), Mx * My );
                traj_adi_int = (double *) calloc(sizeof(double), Mx * My  );
                traj_adi_int_tlp = (double *) calloc(sizeof(double), Mx * My  );

                traj_analytic = (double *) calloc(sizeof(double), Mx * My  );

                // parameter objects
                param params_adi_scalar, params_adi_vector, params_adi_int, params_adi_ispc2, params_adi_int_tlb, params_analytic;
                
                profile_timer_t timer1, timer2, timer3, timer4, timer5; 

                // Alternating Implicit Direction Solver
                params_adi_scalar.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_scalar,1,"save_adi_scalar.txt",1) ;
                params_adi_vector.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_vector,1,"save_adi_vector.txt",1) ;
                params_adi_int.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int,1,"save_adi_int.txt",1) ;
                params_adi_int_tlb.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int_tlp,1,"save_adi_int_tlp.txt",1) ;
                params_adi_ispc2.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,0,traj_adi_int_tlp,1,"save_adi_ispc2.txt",1) ;


                params_adi_scalar.set_timer(timer1);
                params_adi_vector.set_timer(timer2);
                params_adi_int.set_timer(timer3);
                params_adi_int_tlb.set_timer(timer4);
                params_adi_ispc2.set_timer(timer5);

                // Analytic Solver
                //params_analytic.set_params(Dconst,xstart,ystart,Mx,My,delta_x,delta_t,total_t,2,traj_analytic,1,"save_analytic.txt",1);

                // Solver objects
                adi_solver solver;


                // Solve

                double opcount = 24.*(M-2.)*(M-2.);
                double memory_traffic = 2.*(M-2.)*M;

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);                
                solver.solve_scalar(grid, params_adi_scalar);
                double dur = timer_clock_diff_2(params_adi_scalar.timer); 
                printf("P: Grid: %4d \t %20s %4.4f \n",M,"Scalar:",dur);
                printf("P: Perf: %4d \t %20s %4.4f \n",M,"Scalar:",opcount/(dur*FREQUENCY_COEFFICIENT));
                printf("P: BW  : %4d \t %20s %4.4f \n",M,"Scalar:",(memory_traffic*8)/(dur*FREQUENCY_COEFFICIENT));
                timer_reset(params_adi_scalar.timer);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);        
                solver.solve_scalar_unroll(grid, params_adi_scalar);
                dur = timer_clock_diff_2(params_adi_scalar.timer); 
                printf("P: Grid: %4d \t %20s %4.4f \n",M,"Unroll:",dur);
                printf("P: Perf: %4d \t %20s %4.4f \n",M,"Unroll:",opcount/(dur*FREQUENCY_COEFFICIENT));
                printf("P: BW  : %4d \t %20s %4.4f \n",M,"Unroll:",(memory_traffic*8)/(dur*FREQUENCY_COEFFICIENT));
                timer_reset(params_adi_scalar.timer);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_ispc(grid, params_adi_vector);
                dur = timer_clock_diff_2(params_adi_vector.timer); 
                printf("P: Grid: %4d \t %20s %4.4f \n",M,"ISPC:",dur);
                printf("P: Perf: %4d \t %20s %4.4f\n",M,"ISPC:",opcount/(dur*FREQUENCY_COEFFICIENT));
                printf("P: BW  : %4d \t %20s %4.4f \n",M,"ISPC:",(memory_traffic*8)/(dur*FREQUENCY_COEFFICIENT));
                timer_reset(params_adi_vector.timer);

                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_int(grid, params_adi_int);
                dur = timer_clock_diff_2(params_adi_int.timer); 
                printf("P: Grid: %4d \t %20s %4.4f \n",M,"ISPC+INT:",dur);
                printf("P: Perf: %4d \t %20s %4.4f \n",M,"ISPC+INT:",opcount/(dur*FREQUENCY_COEFFICIENT));
                printf("P: BW  : %4d \t %20s %4.4f \n",M,"ISPC+INT:",(memory_traffic*8)/(dur*FREQUENCY_COEFFICIENT));
                //timer_reset(params_adi_int.timer);


                set_bc(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                set_ic(grid,xstart,ystart,delta_x,delta_y,Mx,My);
                solver.solve_vector_ispc2(grid, params_adi_ispc2);
                dur = timer_clock_diff_2(params_adi_ispc2.timer); 
                printf("P: Grid: %4d \t %20s %4.4f \n",M,"ISPC2:",dur);
                printf("P: Perf: %4d \t %20s %4.4f \n",M,"ISPC2:",opcount/(dur*FREQUENCY_COEFFICIENT));
                printf("P: BW  : %4d \t %20s %4.4f \n",M,"ISPC2:",(memory_traffic*8)/(dur*FREQUENCY_COEFFICIENT));
                //timer_reset(params_adi_ispc2.timer);


                // free memmory
                free(traj_adi_scalar);
                free(traj_adi_vector);
                free(traj_adi_int);
                free(traj_adi_int_tlp);
                free(traj_analytic);
                free(grid);
    }

    return 0;


}
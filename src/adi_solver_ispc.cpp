#include "adi_solver.h"
void adi_solver::solve_vector_ispc(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    //double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;

    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    double *r_x = (double *) calloc(sizeof(double), (Mx - 2)*UNROLL );
    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);
    double *r_y = (double *) calloc(sizeof(double), (My - 2)*UNROLL);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);

    while (t <= finish) {

        if (param_vec.save_opt == 1) {
            copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
            counter += 1;
            print_grid_to_file(param_vec.save_file, grid, Mx, t);
        }
        else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
        }
        #ifdef VERBOSE
                printf("### Time: %f\n", t);
        #endif


        // ------------- Implicit x ----------------------

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_tic(param_vec.timer);
            }
        #endif

        for (int j =1  ; j < (My - 1); j++) {

            ispc::adi_stencil_1(Mx,My,constant,grid + j,r_x);

            //custom thomas execution
            r_x[0] = r_x[0]/b;
            for (int i = 1; i<Mx-2; i++){
                r_x[i] =  (r_x[i] - a*r_x[i-1])*(c_denom_x[i-1]);
            }

            rho_new[Mx-2][j] = r_x[Mx-2-1];
            for (int i = Mx-2-2; i>=0 ; i--){
                rho_new[i+1][j] = r_x[i]- c_prime_x[i]*rho_new[i+2][j];
            }

        }   

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
               timer_toc(param_vec.timer);
            }
        #endif

        // ------------- Implicit y --------------------------
        for (int i = 1; i < Mx -1; i++){
            ispc::adi_stencil_2(Mx,My,constant, mem + i*Mx,r_y);
            spec_thomas_mult_denom( grid + i*Mx + 1 , r_y, c_prime_y, c_denom_y, a, b, My-2);
        }

        #ifdef TIME_MEASUREMENT
            if (param_vec.timer_on==true){
                timer_toc(param_vec.timer);
            }
        #endif
        t = t + dt;

    }
    printf("### Simulation Stopped with ADI\n");

    free(r_x);
    free(r_y);
    free(c_denom_x);
    free(c_denom_y);
    free(c_prime_x);
    free(c_prime_y);

    free(mem);
}































/*
***********************************************************************************
***********************************************************************************
*/


void adi_solver::solve_vector_ispc_tlp(double *grid, param & param_vec) {

    const int Mx = param_vec.Mx;
    const int My = param_vec.My;
    const double D = param_vec.D;
    const double h = param_vec.space_step;

    //double (*rho)[My] = (double (*)[My]) grid;

    double *mem = (double *) calloc(sizeof(double), Mx * My);
    double (*rho_new)[My] = (double (*)[My]) mem;

    int counter = 0;
    double t = 0;
    const double dt = param_vec.time_step;
    const double finish = param_vec.total_time;

    printf("### Simulation Started with ADI\n");

    const double b = (-1.) * (1. + D * dt / (h * h));
    const double a = D * dt / (2. * h * h);
    const double c = D * dt / (2. * h * h);

    double *c_prime_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_x = (double *) calloc(sizeof(double), Mx - 3);
    double *c_denom_y = (double *) calloc(sizeof(double), My - 3);
    double *c_prime_y = (double *) calloc(sizeof(double), My - 3);

    const double constant = (D * dt / (2 * h * h));

    // First part of Thomas Algorithm, done once
    spec_thomas_invert(a, b, c, c_prime_x, Mx-2);
    spec_thomas_invert(a, b, c, c_prime_y, My-2);
    spec_thomas_denom(a,b,c_prime_x,c_denom_x,Mx-2);
    spec_thomas_denom(a,b,c_prime_y,c_denom_y,My-2);

#pragma omp parallel
    {

        while (t <= finish) {
            #pragma omp single
            {
            if (param_vec.save_opt == 1) {
                copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
                counter += 1;
                print_grid_to_file(param_vec.save_file, grid, Mx, t);
            }
            else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
            }
            #ifdef VERBOSE
                        printf("### Time: %f\n", t);
            #endif
            }

            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                   timer_tic(param_vec.timer);
                }
            #endif
            }

            // ------------- Implicit x ----------------------

            #pragma omp for
            for (int j = 1; j < My - 1; j++) {
                double *r_x = (double *) calloc(sizeof(double), Mx - 2);

                ispc::adi_stencil_1(Mx, My, constant, grid + j, r_x);

               //custom thomas execution
                r_x[0] = r_x[0] / b;
                for (int i = 1; i < Mx - 2; i++) {
                    r_x[i] = (r_x[i] - a * r_x[i - 1]) / (b - a * c_prime_x[i - 1]);
                }

                rho_new[Mx - 2][j] = r_x[Mx - 2 - 1];
                for (int i = Mx - 2 - 2; i >= 0; i--) {
                    rho_new[i + 1][j] = r_x[i] - c_prime_x[i] * rho_new[i + 2][j];
                }

                free(r_x);
            }


            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                   timer_toc(param_vec.timer);
                }
            #endif
            }

            // ------------- Implicit y --------------------------

            #pragma omp for
            for (int i = 1; i < Mx - 1; i++) {
                double *r_y = (double *) calloc(sizeof(double), My - 2);

                ispc::adi_stencil_2(Mx, My, constant, mem + i * My,r_y);
                spec_thomas_mult(grid + i * Mx + 1, r_y, c_prime_y, a, b, My - 2);
                // no copy needed as no strided access
                free(r_y);

            }

            #pragma omp single
            {
            #ifdef TIME_MEASUREMENT
                if (param_vec.timer_on==true){
                   timer_toc(param_vec.timer);
                }
            #endif
            }

            #pragma omp single
            {
                t = t + dt;
            }
        }
    }
    printf("### Simulation Stopped with ADI\n");

    free(c_prime_x);
    free(c_prime_y);
    free(c_denom_x);
    free(c_denom_y);
    free(mem);
}

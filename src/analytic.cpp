//
// Created by mojko on 7/24/16.
//

#include "analytic.h"


void analytic::evaluate(double t, double * grid, double D, int Mx, int My, double xstart,double ystart, double h){


    auto trans_x = [h,xstart](int j) {
        return (double)  h * (double) j + xstart;
    };
    auto trans_y = [h,ystart] (int j) {
        return (double) h * (double) j + ystart;
    };


    double (* rho)[My] = (double (*)[My]) grid;

    //#pragma omp for
    for (int i = 1; i<(Mx-1); i++){
        for (int j = 1; j<(My-1); j++){
            rho[i][j] = sin(M_PI*trans_x(i))*sin(M_PI*trans_y(j))*exp(-2*D*M_PI*M_PI*t);
        }
    }


};

void analytic::evaluate_trajectory(double * grid, param param_vec) {

    double t = 0;
    double dt = param_vec.time_step;
    double finish = param_vec.total_time;
    double h = param_vec.space_step;

    int Mx = param_vec.Mx;
    int My = param_vec.My;
    double xstart = param_vec.x_start;
    double ystart = param_vec.y_start;

    double D = param_vec.D;
    int counter = 0;


    printf("### Simulation Started with Analytic\n");
/*#pragma omp parallel
 {*/

    while (t < finish) {
        this->evaluate(t, grid, D, Mx, My, xstart, ystart, h);
        
        //#pragma omp single
        //{
        if (param_vec.save_opt == 1) {
            // copy grid to traj
            copy_2d(grid, param_vec.trajectory + counter * Mx * My, Mx, My);
            counter += 1;
            print_grid_to_file(param_vec.save_file, grid, Mx, t);
        }
        else if (param_vec.save_opt == 2){
            copy_2d(grid, param_vec.trajectory, Mx, My);
        }
        t = t + dt;
        //}

    }
//}
    printf("### Simulation Stopped with Analytic\n");



}
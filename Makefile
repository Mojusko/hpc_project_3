export MAKE=make
export OBJDIR=bin
export BUILDDIR=build
export INCLUDEDIR=include
export OTHERDIR=other
export HPP_FILES=$(wildcard include/*.h)
export CPP_FILES=$(wildcard src/*.cpp)
export OBJ_FILES=$(wildcard $(OBJDIR)/*.o)
export GNU_FILES=$(wildcard plot_scripts/*.gnu)


export EIGEN_DIR=/usr/include/eigen3

export SRC_FILES_ADI_TEST_1 = $(filter-out src/main.cpp, $(CPP_FILES)) src/tests/test_adi.cpp
export SRC_FILES_ADI_TEST_2 = $(filter-out src/main.cpp, $(CPP_FILES)) src/tests/test_adi_2.cpp
export SRC_FILES_PSE_TEST_1 = $(filter-out src/main.cpp, $(CPP_FILES)) src/tests/test_pse.cpp
export SRC_FILES_PSE_TEST_2 = $(filter-out src/main.cpp, $(CPP_FILES)) src/tests/test_pse_2.cpp

export SRC_FILES_ADI_PROFILE_1 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_adi.cpp
export SRC_FILES_ADI_PROFILE_2 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_adi_parallel.cpp
export SRC_FILES_ADI_PROFILE_3 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_adi_parallel_2.cpp
export SRC_FILES_ADI_PROFILE_4 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_adi_parallel_strong_scale.cpp
export SRC_FILES_ADI_PROFILE_5 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_adi_split.cpp

export SRC_FILES_PSE_PROFILE_1 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_pse.cpp
export SRC_FILES_PSE_PROFILE_2 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_pse_parallel.cpp
export SRC_FILES_PSE_PROFILE_3 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_pse_parallel_2.cpp
export SRC_FILES_PSE_PROFILE_4 = $(filter-out src/main.cpp, $(CPP_FILES)) src/profiles/profile_pse_parallel_strong_scale.cpp

export SRC_FILES_CACHE_PROFILE_1 = $(filter-out src/main.cpp, $(CPP_FILES)) src/cache_tests/cache_test_scalar.cpp
export SRC_FILES_CACHE_PROFILE_2 = $(filter-out src/main.cpp, $(CPP_FILES)) src/cache_tests/cache_test_parallel.cpp



CC=CC
OPT=-O3
ISPC_OPT=-O3 --opt=fast-math
ISPC=ispc/ispc-v1.9.1-linux/ispc

ifeq ($(CC),g++)
CFLAGS=-std=c++11 $(OPT) -I $(EIGEN_DIR) -I $(INCLUDEDIR) -Wall  -funroll-loops -fstrict-aliasing -march=native -fopenmp -mfma -g
else
CFLAGS=-std=c++11 $(OPT) -I $(EIGEN_DIR) -I $(INCLUDEDIR) -Wall  -funroll-loops -fstrict-aliasing -march=native -fopenmp -mfma -g
endif 

#ifndef DEBUG
#CFLAGS=-std=c++11 $(OPT) -I $(EIGEN_DIR) -I $(INCLUDEDIR) -Wall  -fstrict-aliasing -march=native -fopenmp -g -stdlib=libc++ -std=gnu++11
#else
#CFLAGS=-std=c++11 -I $(EIGEN_DIR) -I $(INCLUDEDIR) -g -Wall  -fstrict-aliasing -march=native -fopenmp 
#endif 


dir:
	mkdir -p $(OBJDIR)
	mkdir -p $(BUILDDIR)

all: dir
	make ispc
	make code
	cp $(OTHERDIR)/compare.py $(BUILDDIR)/.
	cp $(OTHERDIR)/plotter.py $(BUILDDIR)/.

ispc: ./bin/adi_stencil.o ./bin/pse_stencil.o ./bin/adi_full.o

./bin/adi_stencil.o: ispc_kernels/adi_stencil.ispc 
	$(ISPC) $(ISPC_OPT) -I $(INCLUDEDIR) --target=avx2 ispc_kernels/adi_stencil.ispc -h include/adi_stencil.h -o bin/adi_stencil.o
./bin/pse_stencil.o: ispc_kernels/pse_stencil.ispc 
	$(ISPC) $(ISPC_OPT) -I $(INCLUDEDIR) --target=avx2 ispc_kernels/pse_stencil.ispc -h include/pse_stencil.h -o bin/pse_stencil.o
./bin/adi_full.o: ispc_kernels/adi_full.ispc 
	$(ISPC) $(ISPC_OPT) -I $(INCLUDEDIR) --target=avx2 ispc_kernels/adi_full.ispc -h include/adi_full.h -o bin/adi_full.o


code: ./$(BUILDDIR)/diff_solver

./$(BUILDDIR)/diff_solver: $(CPP_FILES) $(HPP_FILES)
	$(CC) $(OPT) $(CFLAGS) $(CPP_FILES) $(OBJ_FILES) -o $(BUILDDIR)/diff_solver

clean:	
	rm -r $(OBJDIR)
	rm -r $(BUILDDIR)
	rm include/adi_stencil.h
	rm include/pse_stencil.h
	rm include/adi_full.h

run_adi: all
	./$(BUILDDIR)/diff_solver 
	python3 $(BUILDDIR)/compare.py save_analytic.txt save_adi.txt

run_pse: all
	./$(BUILDDIR)/diff_solver 
	python3 $(BUILDDIR)/compare.py save_analytic.txt save_pse.txt

test_adi: ispc $(CPP_FILES) $(HPP_FILES)
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_TEST_1) $(OBJ_FILES) -o $(BUILDDIR)/test_adi
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_TEST_2) $(OBJ_FILES) -o $(BUILDDIR)/test_adi2
	#./$(BUILDDIR)/test_adi | grep T: > results/adi_test.txt
	#./$(BUILDDIR)/test_adi2 | grep T: > results/adi_test2.txt

test_pse: ispc $(CPP_FILES) $(HPP_FILES)
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_TEST_1) $(OBJ_FILES) -o $(BUILDDIR)/test_pse
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_TEST_2) $(OBJ_FILES) -o $(BUILDDIR)/test_pse2
	#./$(BUILDDIR)/test_pse | grep T: > results/pse_test.txt

cache_test: ispc $(CPP_FILES) $(HPP_FILES)
	$(CC) $(OPT) $(CFLAGS)  $(SRC_FILES_CACHE_PROFILE_1) $(OBJ_FILES) -o $(BUILDDIR)/cache_scalar
	$(CC) $(OPT) $(CFLAGS)  $(SRC_FILES_CACHE_PROFILE_2) $(OBJ_FILES) -o $(BUILDDIR)/cache_parallel



profile_adi: $(BUILDDIR)/profile_adi $(BUILDDIR)/profile_adi_parallel $(BUILDDIR)/profile_adi_parallel_2 $(BUILDDIR)/profile_adi_parallel_weak_scale $(BUILDDIR)/profile_adi_split

$(BUILDDIR)/profile_adi: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_PROFILE_1) $(OBJ_FILES) -o $(BUILDDIR)/profile_adi
$(BUILDDIR)/profile_adi_parallel: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_PROFILE_2) $(OBJ_FILES) -o $(BUILDDIR)/profile_adi_parallel
$(BUILDDIR)/profile_adi_parallel_2: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_PROFILE_3) $(OBJ_FILES) -o $(BUILDDIR)/profile_adi_parallel_2
$(BUILDDIR)/profile_adi_parallel_weak_scale: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_PROFILE_4) $(OBJ_FILES) -o $(BUILDDIR)/profile_adi_parallel_weak_scale
$(BUILDDIR)/profile_adi_split: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_ADI_PROFILE_5) $(OBJ_FILES) -o $(BUILDDIR)/profile_adi_split

profile_pse: $(BUILDDIR)/profile_pse $(BUILDDIR)/profile_pse_parallel $(BUILDDIR)/profile_pse_parallel_2 $(BUILDDIR)/profile_pse_parallel_weak_scale

$(BUILDDIR)/profile_pse: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_PROFILE_1) $(OBJ_FILES) -o $(BUILDDIR)/profile_pse
$(BUILDDIR)/profile_pse_parallel: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_PROFILE_2) $(OBJ_FILES) -o $(BUILDDIR)/profile_pse_parallel
$(BUILDDIR)/profile_pse_parallel_2: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_PROFILE_3) $(OBJ_FILES) -o $(BUILDDIR)/profile_pse_parallel_2
$(BUILDDIR)/profile_pse_parallel_weak_scale: ispc $(CPP_FILES) $(HPP_FILES) dir
	$(CC) $(OPT) $(CFLAGS) $(SRC_FILES_PSE_PROFILE_4) $(OBJ_FILES) -o $(BUILDDIR)/profile_pse_parallel_weak_scale

plots: phony
	gnuplot plot_scripts/adi_cvg.gnu
	gnuplot plot_scripts/adi_perf.gnu
	gnuplot plot_scripts/adi_strong_scalling.gnu
	gnuplot plot_scripts/adi_weak_scalling.gnu
	gnuplot plot_scripts/pse_cvg.gnu
	gnuplot plot_scripts/pse_perf.gnu
	gnuplot plot_scripts/pse_strong_scalling.gnu
	gnuplot plot_scripts/pse_weak_scalling.gnu
	gnuplot plot_scripts/adi_perf_split.gnu
	gnuplot plot_scripts/pse_runtime.gnu
	gnuplot plot_scripts/adi_roofline.gnu
	gnuplot plot_scripts/pse_roofline.gnu
	gnuplot plot_scripts/roofline_dora.gnu
	gnuplot plot_scripts/tlb_adi.gnu

phony: 
	true

unroll: phony
	#python other/unroll.py src/adi_solver_scalar.cpp 8
	python other/unroll_ispc.py ispc_kernels/adi_full.ispc 8  

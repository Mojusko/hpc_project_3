CrayPat/X:  Version 6.3.2 Revision rc1/6.3.2  02/25/16 18:26:21

Number of PEs (MPI ranks):    1
                           
Numbers of PEs per Node:      1
                           
Numbers of Threads per PE:    7
                           
Number of Cores per Socket:  18

Execution start time:  Sat Aug 13 23:32:28 2016

System name and speed:  nid00478  2101 MHz (approx)

Current path to data file:
  /users/course30/hpc_2_eth_course/Project_3/cache_experiment/cache_scalar+7382-478s.ap2  (RTS)


Notes for table 1:

  Table option:
    -O samp_profile
  Options implied by table option:
    -d sa%@0.95,sa,imb_sa,imb_sa% -b gr,fu,th=HIDE

  Options for related tables:
    -O samp_profile+src    

  The Total value for Samp is the sum for the Group values.
  The Group value for Samp is the sum for the Function values.
  The Function value for Samp is the select0 for the Thread values.
    (To specify different aggregations, see: pat_help report options s1)

  This table shows only lines with Samp% > 0.95.
    (To set thresholds to zero, specify:  -T)

  Imbalance percentages are relative to a set of threads or PEs.
  Other percentages at each level are of the Total for the program.
    (For percentages relative to next level up, specify:
      -s percent=r[elative])

  To make the profile easier to interpret, some samples are attributed
  to a caller that is either a user defined function, or a library
  function called directly by a user defined function.  To disable this
  adjustment, and show functions actually sampled, use the -P option.
  
  The following groups were pruned due to thresholding:
    HEAP

Table 1:  Profile by Function

  Samp% |  Samp | Imb. |  Imb. |Group
        |       | Samp | Samp% | Function
        |       |      |       |  Thread=HIDE
       
 100.0% | 199.0 |   -- |    -- |Total
|---------------------------------------------------------
| 100.0% | 199.0 |   -- |    -- |ETC
||--------------------------------------------------------
||  53.3% | 106.0 |   -- |    -- |GOMP_parallel
||  13.1% |  26.0 |  8.0 | 28.3% |__memset_sse2
||   9.0% |  18.0 | 29.3 | 67.0% |gomp_team_barrier_wait_end
||   8.0% |  16.0 |  3.1 | 19.3% |adi_full_1
||   6.5% |  13.0 |  3.6 | 27.8% |adi_full_2
||   3.5% |   7.0 |   -- |    -- |main
||   3.5% |   7.0 |   -- |    -- |__lll_timedwait_tid
||   2.5% |   5.0 |  3.2 | 42.2% |_int_free
|=========================================================

Notes for table 2:

  Table option:
    -O samp_profile+src
  Options implied by table option:
    -d sa%@0.95,sa,imb_sa,imb_sa% -b gr,fu,so,li,th=HIDE

  Options for related tables:
    -O samp_profile        

  The Total value for Samp is the sum for the Group values.
  The Group value for Samp is the sum for the Function values.
  The Function value for Samp is the sum for the Source values.
  The Source value for Samp is the sum for the Line values.
  The Line value for Samp is the select0 for the Thread values.
    (To specify different aggregations, see: pat_help report options s1)

  This table shows only lines with Samp% > 0.95.
    (To set thresholds to zero, specify:  -T)

  Imbalance percentages are relative to a set of threads or PEs.
  Other percentages at each level are of the Total for the program.
    (For percentages relative to next level up, specify:
      -s percent=r[elative])

  To make the profile easier to interpret, some samples are attributed
  to a caller that is either a user defined function, or a library
  function called directly by a user defined function.  To disable this
  adjustment, and show functions actually sampled, use the -P option.
  
  The following groups were pruned due to thresholding:
    HEAP

Table 2:  Profile by Group, Function, and Line

  Samp% |  Samp | Imb. |  Imb. |Group
        |       | Samp | Samp% | Function
        |       |      |       |  Source
        |       |      |       |   Line
        |       |      |       |    Thread=HIDE
       
 100.0% | 199.0 |   -- |    -- |Total
|-----------------------------------------------------------------------------
| 100.0% | 199.0 |   -- |    -- |ETC
||----------------------------------------------------------------------------
||  53.3% | 106.0 |   -- |    -- |GOMP_parallel
3|        |       |      |       | ../cray-gcc-5.1.0/libgomp/parallel.c
||||--------------------------------------------------------------------------
4|||   1.0% |   2.0 |   -- |    -- |line.168
4|||  52.3% | 104.0 |   -- |    -- |line.170
||||==========================================================================
||  13.1% |  26.0 |   -- |    -- |__memset_sse2
3|        |       |      |       | x86_64/multiarch/../memset.S
4|  12.1% |  24.0 | 14.4 | 60.0% |  line.879
||   9.0% |  18.0 |   -- |    -- |gomp_team_barrier_wait_end
3|        |       |      |       | libgomp/config/linux/wait.h
||||--------------------------------------------------------------------------
4|||   3.5% |   7.0 |  9.9 | 63.9% |line.54
4|||   5.5% |  11.0 | 18.7 | 68.2% |line.55
||||==========================================================================
||   8.0% |  16.0 |  3.1 | 19.3% |adi_full_1
||   6.5% |  13.0 |  3.6 | 27.8% |adi_full_2
||   3.5% |   7.0 |   -- |    -- |main
3|        |       |      |       | Project_3/src/cache_tests/cache_test_scalar.cpp
4|        |       |      |       |  line.106
||   3.5% |   7.0 |   -- |    -- |__lll_timedwait_tid
3|        |       |      |       | sysv/linux/x86_64/lowlevellock.S
4|        |       |      |       |  line.434
||   2.5% |   5.0 |   -- |    -- |_int_free
3|        |       |      |       | BUILD/glibc-2.11.3/malloc/arena.c
4|   2.0% |   4.0 |  3.3 | 44.4% |  line.821
|=============================================================================

Notes for table 3:

  Table option:
    -O hwpc
  Options implied by table option:
    -d P -b th=HIDE -s show_data=rows

  The Total value for each data item is the select0 for the Thread values.
    (To specify different aggregations, see: pat_help report options s1)

  
  Collection of each of the following performance counters was
  attempted, but all of the values recorded in the data file were zero,
  so the counter and all derived counters that depend on it were filtered
  from this table:
    FP_ARITH:SCALAR_SINGLE
    FP_ARITH:128B_PACKED_DOUBLE
    FP_ARITH:128B_PACKED_SINGLE
    FP_ARITH:256B_PACKED_SINGLE

Table 3:  Program HW Performance Counter Data

Thread=HIDE

  
==============================================================================
  Total
------------------------------------------------------------------------------
  CPU_CLK_THREAD_UNHALTED:THREAD_P                3,898,136,934 
  CPU_CLK_THREAD_UNHALTED:REF_XCLK                  156,417,538 
  DTLB_LOAD_MISSES:MISS_CAUSES_A_WALK                15,572,291 
  DTLB_STORE_MISSES:MISS_CAUSES_A_WALK               24,569,776 
  L1D:REPLACEMENT                                   208,260,773 
  L2_RQSTS:ALL_DEMAND_DATA_RD                        59,477,451 
  L2_RQSTS:DEMAND_DATA_RD_HIT                        32,986,417 
  MEM_UOPS_RETIRED:ALL_LOADS                        855,557,318 
  FP_ARITH:SCALAR_DOUBLE                            712,039,296 
  FP_ARITH:256B_PACKED_DOUBLE                       151,711,445 
  CPU_CLK                            2.49GHz                    
  HW FP Ops / User time           665.976M/sec    1,318,885,076 ops
  Total DP ops                    665.976M/sec    1,318,885,076 ops
  Computational intensity                ops/cycle         1.54 ops/ref
  MFLOPS (aggregate)               665.98M/sec                  
  TLB utilization                   21.31 refs/miss        0.04 avg uses
  D1 cache hit,miss ratios          75.7% hits            24.3% misses
  D1 cache utilization (misses)      4.11 refs/miss        0.51 avg hits
  D2 cache hit,miss ratio           87.3% hits            12.7% misses
  D1+D2 cache hit,miss ratio        96.9% hits             3.1% misses
  D1+D2 cache utilization           32.30 refs/miss        4.04 avg hits
  D2 to D1 bandwidth            1,833.091MiB/sec  3,806,556,864 bytes
==============================================================================

Notes for table 4:

  Table option:
    -O write_stats
  Options implied by table option:
    -d wt,wb,wR,wr@,wC -b fi[max15]

  The Total value for each data item is the sum for the File Name values.
    (To specify different aggregations, see: pat_help report options s1)

  This table shows only lines with Writes > 0.
  This table shows only the maximum 15 File Name entries, sorted by
    Write Time.

Table 4:  File Output Stats by Filename (maximum 15 shown)

    Write |    Write | Write Rate | Writes | Bytes/ |File Name[max15]
     Time |   MBytes | MBytes/sec |        |   Call |
         
 0.000027 | 0.000427 |  15.881883 |   14.0 |  32.00 |Total
|--------------------------------------------------------------------
| 0.000027 | 0.000427 |  15.881883 |   14.0 |  32.00 |stdout
|====================================================================

Notes for table 5:

  Table option:
    -O program_time
  Options implied by table option:
    -d pt,hm -b th

  The Total value for Process HiMem (MBytes), Process Time is the select0 for the Thread values.
    (To specify different aggregations, see: pat_help report options s1)

  The value shown for Process HiMem is calculated from information in
  the /proc/self/numa_maps files captured near the end of the program. 
  It is the total size of all pages, including huge pages, that were
  actually mapped into physical memory from both private and shared
  memory segments.

Table 5:  Wall Clock Time, Memory High Water Mark

  Process |  Process |Thread
     Time |    HiMem |
          | (MBytes) |
         
 2.098067 |     6.81 |Total
|-----------------------------
| 2.098067 |     6.81 |thread.0
|=============================

========================  Additional details  ========================

Experiment:  samp_cs_time

Sampling interval:  10000 microsecs

Original path to data file:
  /users/course30/hpc_2_eth_course/Project_3/cache_experiment/cache_scalar+7382-478s.xf  (RTS)

Original program:
  /users/course30/hpc_2_eth_course/Project_3/build/cache_scalar+orig

Instrumented with:
  pat_build -f -O lite/sample_profile -Drtenv=PAT_RT_REPORT_METHOD=pe0 \
    build/cache_scalar+orig build/cache_scalar

  Option file "lite/sample_profile" contained:
    -Drtenv=PAT_RT_PERFCTR=default_samp
    -Drtenv=PAT_RT_EXPERIMENT=samp_cs_time
    -Drtenv=PAT_RT_SAMPLING_MODE=3
    -Dreport=y
    -Drtenv=PAT_RT_REPORT_CMD=pat_report,-O,lite/sample_profile_rpt,-s,summoner=rtl
    -g upc
    -g caf
    -g mpi
    -g shmem
    -g syscall
    -g io

Instrumented program:
  /users/course30/hpc_2_eth_course/Project_3/cache_experiment/../build/cache_scalar

Program invocation:
  /users/course30/hpc_2_eth_course/Project_3/cache_experiment/../build/cache_scalar 7

Exit Status:  0 for 1 PE

Intel broadwell CPU  Family:  6  Model: 79  Stepping:  1

Thread start functions:
     1 thread:  main
     6 threads:  gomp_thread_start

Memory pagesize:  4 KiB

Memory hugepagesize:  0 B

Programming environment:  GNU

Runtime environment variables:
  ATP_HOME=/opt/cray/atp/2.0.0
  ATP_IGNORE_SIGTERM=1
  ATP_MRNET_COMM_PATH=/opt/cray/atp/2.0.0/libexec/atp_mrnet_commnode_wrapper
  ATP_POST_LINK_OPTS=-Wl,-L/opt/cray/atp/2.0.0/libApp/ 
  CRAYOS_VERSION=5
  CRAYPE_VERSION=2.5.4
  CRAY_LIBSCI_VERSION=16.03.1
  DVS_VERSION=0.9.0
  GCC_VERSION=5.1.0
  GNU_VERSION=5.1.0
  LIBSCI_VERSION=16.03.1
  MODULE_VERSION=3.2.10
  MODULE_VERSION_STACK=3.2.10
  MPICH_ABORT_ON_ERROR=1
  MPICH_DIR=/opt/cray/mpt/7.3.2/gni/mpich-gnu/5.1
  OMP_DYNAMIC=FALSE
  OMP_NUM_THREADS=36
  PATH=/opt/slurm/munge/bin:/opt/cray/llm/default/bin:/opt/cray/llm/default/etc:/opt/cray/lustre-cray_ari_s/2.5_3.0.101_0.46.1_1.0502.8871.16.1-1.0502.21345.8.1/sbin:/opt/cray/lustre-cray_ari_s/2.5_3.0.101_0.46.1_1.0502.8871.16.1-1.0502.21345.8.1/bin:/opt/cray/sdb/1.1-1.0502.63652.4.25.ari/bin:/opt/cray/nodestat/2.2-1.0502.60539.1.31.ari/bin:/opt/cray/perftools/6.3.2/bin:/opt/cray/papi/5.4.3.1/bin:/opt/cray/rca/1.0.0-2.0502.60530.1.62.ari/bin:/opt/cray/alps/5.2.4-2.0502.9822.32.1.ari/sbin:/opt/cray/dvs/2.5_0.9.0-1.0502.2188.1.116.ari/bin:/opt/cray/xpmem/0.1-2.0502.64982.5.3.ari/bin:/opt/cray/pmi/5.0.10-1.0000.11050.0.0.ari/bin:/opt/cray/ugni/6.0-1.0502.10863.8.29.ari/bin:/opt/cray/udreg/2.3.2-1.0502.10518.2.17.ari/bin:/opt/gcc/5.1.0/bin:/apps/common/UES/SLES11/ddt/6.1/libexec:/apps/common/UES/SLES11/ddt/6.1/bin:/opt/cray/mpt/7.3.2/gni/bin:/apps/dora/munge/default/bin:/apps/dora/slurm/default/bin:/opt/slurm/default/bin:/opt/cray/craype/2.5.4/bin:/opt/cray/switch/1.0-1.0502.60522.1.61.ari/bin:/opt/cray/eslogin/eswrap/1.1.0-1.020200.1231.0/bin:/opt/modules/3.2.10.3/bin:/users/course30/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/X11R6/bin:/usr/games:/usr/lib/mit/bin:/usr/lib/mit/sbin:/sbin:/usr/sbin:.:/usr/lib/qt3/bin:/opt/cray/bin:/apps/ela/system/bin:/apps/common/system/bin:/apps/dora/system/bin:/apps/ela/system/bin:/apps/common/system/bin:/apps/dora/system/bin
  PAT_BUILD_PAPI_BASEDIR=/opt/cray/papi/5.4.3.1
  PAT_REPORT_PRUNE_NAME=_cray$mt_start_,__cray_hwpc_,f_cray_hwpc_,cstart,__pat_,pat_region_,PAT_,OMP.slave_loop,slave_entry,_new_slave_entry,__libc_start_main,_start,__start,start_thread,__wrap_,UPC_ADIO_,_upc_,upc_,__caf_,__pgas_,syscall
  PAT_RT_EXPERIMENT=samp_cs_time
  PAT_RT_PERFCTR=default_samp
  PAT_RT_REPORT_CMD=pat_report,-O,lite/sample_profile_rpt,-s,summoner=rtl
  PAT_RT_REPORT_METHOD=pe0
  PAT_RT_SAMPLING_MODE=3
  PERFTOOLS_VERSION=6.3.2
  XTOS_VERSION=5.2.82

Report time environment variables:
    CRAYPAT_ROOT=/opt/cray/perftools/6.3.2
    PAT_REPORT_PRUNE_NAME=_cray$mt_start_,__cray_hwpc_,f_cray_hwpc_,cstart,__pat_,pat_region_,PAT_,OMP.slave_loop,slave_entry,_new_slave_entry,__libc_start_main,_start,__start,start_thread,__wrap_,UPC_ADIO_,_upc_,upc_,__caf_,__pgas_,syscall

Number of MPI control variables collected:  0

  (To see the list, specify: -s mpi_cvar=show)

Report command line options:  <none>

Operating system:
  Linux 3.0.101-0.46.1_1.0502.8871-cray_ari_c #1 SMP Tue Aug 25 21:41:26 UTC 2015

Hardware performance counter events:
   CPU_CLK_THREAD_UNHALTED:THREAD_P      Count core clock cycles whenever the clock signal on the specificcore is running (not halted):Cycles when thread is not halted
   CPU_CLK_THREAD_UNHALTED:REF_XCLK      Count core clock cycles whenever the clock signal on the specificcore is running (not halted):Count Xclk pulses (100Mhz) when the core is unhalted
   DTLB_LOAD_MISSES:MISS_CAUSES_A_WALK   Data TLB load misses:Misses in all DTLB levels that cause page walks
   DTLB_STORE_MISSES:MISS_CAUSES_A_WALK  Data TLB store misses:Misses in all DTLB levels that cause page walks
   L1D:REPLACEMENT                       L1D cache:L1D Data line replacements
   L2_RQSTS:ALL_DEMAND_DATA_RD           L2 requests:Any data read request to L2 cache
   L2_RQSTS:DEMAND_DATA_RD_HIT           L2 requests:Demand Data Read requests that hit L2 cache
   MEM_UOPS_RETIRED:ALL_LOADS            Memory uops retired (Precise Event):All load uops retired
   FP_ARITH:SCALAR_DOUBLE                Floating-point:Number of scalar double precision floating-point arithmetic instructions (multiply by 1 to get flops)
   FP_ARITH:SCALAR_SINGLE                Floating-point:Number of scalar single precision floating-point arithmetic instructions (multiply by 1 to get flops)
   FP_ARITH:128B_PACKED_DOUBLE           Floating-point:Number of scalar 128-bit packed double precision floating-point arithmetic instructions (multiply by 2 to get flops)
   FP_ARITH:128B_PACKED_SINGLE           Floating-point:Number of scalar 128-bit packed single precision floating-point arithmetic instructions (multiply by 4 to get flops)
   FP_ARITH:256B_PACKED_DOUBLE           Floating-point:Number of scalar 256-bit packed double precision floating-point arithmetic instructions (multiply by 4 to get flops)
   FP_ARITH:256B_PACKED_SINGLE           Floating-point:Number of scalar 256-bit packed single precision floating-point arithmetic instructions (multiply by 8 to get flops)

  This set of HWPC events requires multiplexing, which reduces
  the accuracy of the data collected. If the best possible
  accuracy is needed, you should rerun to collect data for
  smaller sets of events, that do not require multiplexing.

Number of traced functions:  80

  (To see the list, specify:  -s traced_functions=show)


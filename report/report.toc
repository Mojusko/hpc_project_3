\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Numerical Problem}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Notation and General Assumptions}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Computational Algorithms}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Alernating Direction Implicit (ADI) Solver}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Particle Strenght Exchange (PSE) Solver}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Background}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Implementation analysis}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Technologies used}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Implementation and Validation}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Implementation}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Validation}{8}{subsection.4.2}
\contentsline {section}{\numberline {5}Implementation and Optimizations}{9}{section.5}
\contentsline {subsection}{\numberline {5.1}General}{9}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}ADI}{11}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}PSE}{13}{subsection.5.3}
\contentsline {section}{\numberline {6}Discussion and Results}{14}{section.6}
\contentsline {subsection}{\numberline {6.1}ADI}{15}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Perfromance and Roofline analysis}{15}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Parallelization}{15}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Perftool profiling and TLB}{18}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}PSE}{19}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Perfromance and Roofline analysis}{19}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Runtime Comparison}{20}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Parallelization}{20}{subsubsection.6.2.3}
\contentsline {section}{\numberline {7}Conclusion}{22}{section.7}
